
function Auto(p, g) {
    this.pavadinimas = p;
    this.greitis = g;
}
var automobiliai = [];
var forma = document.getElementById('forma');
var data = document.getElementById('auto');
var lenktyniauti = document.getElementById('lenktyniauti');
forma.addEventListener('submit', function (e) {
    e.preventDefault();
    var pavadinimas = document.getElementById('pavadinimas').value;
    var greitis = document.getElementById('greitis').value;
    var auto = new Auto(pavadinimas, greitis);
    automobiliai.push(auto);
    var pavDiv = document.createElement('div');
    pavDiv.innerHTML = pavadinimas;
    var greitDiv = document.createElement('div');
    greitDiv.innerHTML = greitis;
    var autoRow = document.createElement('div');
    autoRow.classList.add('auto-row');
    autoRow.appendChild(pavDiv);
    autoRow.appendChild(greitDiv);
    data.appendChild(autoRow);
});
var rezultatai = document.getElementById('rezultatai');
lenktyniauti.addEventListener('submit', function (e) {
    e.preventDefault();
    var laikas = parseInt(document.getElementById('laikas').value);
    var atstumai = [];
    for (var i = 0; i < automobiliai.length; i++) {
        var atstumas = parseInt(laikas) * parseInt(automobiliai[i].greitis);
        atstumai.push(atstumas);
    }
    var max = Math.max(...atstumai);
    for (let j = 0; j < atstumai.length; j++) {
        var pavDiv = document.createElement('div');
        pavDiv.innerHTML = automobiliai[j].pavadinimas;
        var atstumDiv = document.createElement('div');
        atstumDiv.innerHTML = atstumai[j];
        var autoRow = document.createElement('div');
        autoRow.classList.add('auto-row');
        autoRow.appendChild(pavDiv);
        autoRow.appendChild(atstumDiv);
        rezultatai.appendChild(autoRow);
    }
});