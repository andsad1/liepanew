var figuros = [
    {
        ilgis: 2,
        plotis: 5,
        aukstis: 3
    },
    {
        ilgis: 3,
        plotis: 2,
        aukstis: 6
    },
    {
        ilgis: 1,
        plotis: 5,
        aukstis: 5
    }
];

var pirmas = document.getElementById('pirmas');
var antras = document.getElementById('antras');
pirmas.classList.add('yellow');
antras.classList.add('red');
var tbody = document.querySelector('tbody');

pirmas.addEventListener('click', function () {
    for (var i = 0; i < figuros.length; i++) {

        var tableRow = document.createElement('tr');
        var ilgisTd = document.createElement('td');
        var plotisTd = document.createElement('td');
        var aukstisTd = document.createElement('td');
        var turisTd = document.createElement('td');

        tableRow.appendChild(ilgisTd).innerHTML = figuros[i].ilgis;
        tableRow.appendChild(plotisTd).innerHTML = figuros[i].plotis;
        tableRow.appendChild(aukstisTd).innerHTML = figuros[i].aukstis;
        tableRow.appendChild(turisTd).innerHTML;

        tbody.appendChild(tableRow);
    };
    this.setAttribute('disabled', 'true');
});
antras.addEventListener('click', function () {
    if (tbody.rows.length >= 1) {
        for (var i = 0; i < figuros.length; i++) {
            var turis = figuros[i].ilgis * figuros[i].plotis * figuros[i].aukstis;
            tbody.childNodes[i + 1].childNodes[3].innerHTML = turis;
            var turis1 = tbody.childNodes[1].childNodes[3].innerHTML;
            var turis2 = tbody.childNodes[2].childNodes[3].innerHTML;
            var turis3 = tbody.childNodes[3].childNodes[3].innerHTML;
        }
        this.setAttribute('disabled', 'true');
        if (turis1 > turis2 && turis1 > turis3) {
            tbody.childNodes[1].classList.add('red');
        } else if (turis2 > turis1 && turis2 > turis3) {
            tbody.childNodes[2].classList.add('red');
        } else {
            tbody.childNodes[3].classList.add('red');
        }
    } else {
        alert('Spauskite pradiniai duomenys!');
    }
});