var id, parentId;

function drag(e) {
    id = e.target.id;
    parentId = e.target.parentElement.id;
}
function allowDrop(e) {
    if (e.target.id == id || e.target.id == parentId) return;
    e.preventDefault();
}
function drop(e) {
    e.preventDefault();
    var element = document.getElementById(id);
    e.target.appendChild(element);
}