var table = document.getElementById('lenta');

function tikrintiArTuscia() {
    var tuscias = false;
    var data = document.getElementById('data').value;
    var nr = document.getElementById('nr').value;
    var km = document.getElementById('km').value;
    var laikas = document.getElementById('laikas').value;

    if (data === '') {
        alert('Datos laukas negali būti tuščias!');
        tuscias = true;
    } else if (nr === '') {
        alert('Valstybinis numeris turi būti įvestas!');
        tuscias = true;
    } else if (km === '') {
        alert('Atstumas turi būti įvestas!');
        tuscias = true;
    } else if (laikas === '') {
        alert('Laikas turi būti įvestas!');
        tuscias = true;
    }
    return tuscias;
}
function lentele() {
    var table = document.getElementById('lenta');
    if (!tikrintiArTuscia()) {
        var newRow = table.insertRow(table.length);
        var cell1 = newRow.insertCell(0);
        var cell2 = newRow.insertCell(1);
        var cell3 = newRow.insertCell(2);
        var cell4 = newRow.insertCell(3);

        data = document.getElementById('data').value;
        nr = document.getElementById('nr').value;
        km = document.getElementById('km').value;
        laikas = document.getElementById('laikas').value;

        cell1.innerHTML = data;
        cell2.innerHTML = nr;
        cell3.innerHTML = km;
        cell4.innerHTML = laikas;

        data = document.getElementById('data').value = '';
        data = document.getElementById('nr').value = '';
        data = document.getElementById('km').value = '';
        data = document.getElementById('laikas').value = '';
    }
}

