var id, parentId;

            function drag(e) {
                id = e.target.id;
                parentId = e.target.parentElement.id;
            }
            function allowDrop(e) {
                if (e.target.id == id || e.target.id == parentId) return;
                e.preventDefault();
            }   
            function drop(e) {
                e.preventDefault();
                var element = document.getElementById(id);
                var kertamaFigura = document.getElementById(e.target.id);
                if (id != e.target.id && kertamaFigura.classList.contains('figura')) {
                    var parent = document.getElementById(e.target.parentElement.id);
                    parent.removeChild(kertamaFigura);
                    parent.appendChild(element);
                } else {
                    var parent = document.getElementById(e.target.id);
                    console.log(parent);
                    parent.appendChild(element);
                }

                var steps = document.getElementById('steps');
                var step = document.createElement("div");
                console.log(step);
                step.innerHTML = parentId + '-' + e.target.id;
                steps.appendChild(step);
                console.log('ondrop');
            }