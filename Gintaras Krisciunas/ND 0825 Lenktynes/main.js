var forma1 = document.getElementById('forma1');
var forma2 = document.getElementById('forma2');
var duomenys = document.getElementById('duomenys');
var rezultatai = document.getElementById('rezultatai');
var pradiniai = document.getElementById('pradiniai');
function Car(p, g) {
    this.pavadinimas = p;
    this.greitis = g;
}

var auto = [];
var atstumai = [];

forma1.onsubmit = function(e) {
    e.preventDefault();
    var pav = document.getElementById('pavadinimas');
    var greit = document.getElementById('greitis');
    var automobilis = new Car(pav.value, greit.value);
    var outterDiv = document.createElement('div');
    outterDiv.setAttribute('class', 'ivesta');
    var pavDiv = document.createElement('div');
    var greitDiv = document.createElement('div');
    pavDiv.textContent = automobilis.pavadinimas;
    greitDiv.textContent = automobilis.greitis;
    outterDiv.appendChild(pavDiv);
    outterDiv.appendChild(greitDiv);
    pradiniai.appendChild(outterDiv);
    auto.push(automobilis);
    pav.value = '';
    greit.value = '';    
};

forma2.onsubmit = function(e) {
    e.preventDefault();
    var laikas = document.getElementById('laikas').value;
    for (let i = 0; i < auto.length; i++) {
        var atstumas = auto[i].greitis*laikas;
        atstumai.push(atstumas);
    }

    var max = Math.max(...atstumai);
    for (let i = 0; i < auto.length; i++) {
        var outterDiv = document.createElement('div');
        var pavDiv = document.createElement('div');
        var atstumDiv = document.createElement('div');
        pavDiv.textContent = auto[i].pavadinimas;
        atstumDiv.textContent = atstumai[i]
        if (max == atstumai[i]) {
            outterDiv.classList.add('greiciausias');
        }
        outterDiv.appendChild(pavDiv);
        outterDiv.appendChild(atstumDiv);
        rezultatai.appendChild(outterDiv);
    }
}