document.getElementById("submit").addEventListener("click", addRow);
document.getElementById("submit").addEventListener("click", reset);


function checkInputs() {
    var empty = false;
    var data_laikas = document.getElementById('data_laikas').value;
    var numeris = document.getElementById('numeris').value;
    var atstumas = document.getElementById('atstumas').value;
    var laikas = document.getElementById('laikas').value;

    if (data_laikas === '') {
        alert('Laukelis negali būti tuščias!');
        empty = true;
    } else if (numeris === '') {
        alert('Laukelis negali būti tuščias!');
        empty = true;
    } else if (atstumas === '') {
        alert('Laukelis negali būti tuščias!');
        empty = true;
    } else if (laikas === '') {
        alert('Laukelis negali būti tuščias!');
        empty = true;
    }
    return tuscias;
}

function reset() {
    document.getElementById("forma").reset();
}


function addRow() {  
    var tableBody = document.getElementById("table_body");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");
    var td3 = document.createElement("td");    
    var td4 = document.createElement("td");    
    var row = document.createElement("tr");
    
    td1.innerHTML = document.getElementById("data_laikas").value;
    td2.innerHTML  = document.getElementById("numeris").value;
    td3.innerHTML  = document.getElementById("atstumas").value;
    td4.innerHTML  = document.getElementById("laikas").value;
    
    row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4);
    
    tableBody.appendChild(row);
    }
    
    
   