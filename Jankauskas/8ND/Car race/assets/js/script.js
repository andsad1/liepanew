function Automobiliai (name, speed) {
    this.pavadinimas = name;
    this.greitis = speed;
}

var autoMasyvas = [];

var forma = document.getElementById('cars_and_speed');
var forma2 = document.getElementById('car_time');
var duomenuDetuve = document.getElementById('pradiniai_duomenys');

forma.addEventListener('submit', function(e){
    e.preventDefault();
    var pavadinimas = document.getElementById('auto_name').value;
    var greitis = document.getElementById('auto_speed').value;
    var autoKonstruktorius = new Automobiliai(pavadinimas, greitis);
    autoMasyvas.push(autoKonstruktorius);
    var autoDiv = document.createElement('div');
    autoDiv.innerHTML = pavadinimas;
    var greicioDiv = document.createElement('div');
    greicioDiv.innerHTML = greitis;
    var pirminiuDiv = document.createElement('div');
    pirminiuDiv.classList.add('pirminiu-div');
    pirminiuDiv.appendChild(autoDiv);
    pirminiuDiv.appendChild(greicioDiv);
    duomenuDetuve.appendChild(pirminiuDiv);
    document.getElementById('auto_name').value = "";
    document.getElementById('auto_speed').value = "";
});

var duomenuDetuve2 = document.getElementById('apskaiciuoti_duomenys');

forma2.addEventListener('submit', function(e){
    e.preventDefault();
    var laikas = document.getElementById('time').value;
    var nuvaziuotasKelias = [];
    for (var i = 0; i < autoMasyvas.length; i++){
    var kelias = parseInt(laikas) * parseInt(autoMasyvas[i].greitis);
    nuvaziuotasKelias.push(kelias);
    }
    var max = Math.max(...nuvaziuotasKelias);
    for (var j = 0; j < nuvaziuotasKelias.length; j++){
        var autoDiv = document.createElement('div');
        autoDiv.innerHTML = autoMasyvas[j].pavadinimas;
        var greicioDiv = document.createElement('div');
        greicioDiv.innerHTML = nuvaziuotasKelias[j];
        var pirminiuDiv = document.createElement('div');
        if (nuvaziuotasKelias[j] == max) {
            pirminiuDiv.classList.add('cempionas');
        }
        pirminiuDiv.classList.add('pirminiu-div');
        pirminiuDiv.appendChild(autoDiv);
        pirminiuDiv.appendChild(greicioDiv);
        duomenuDetuve2.appendChild(pirminiuDiv);
        document.getElementById('time').value = "";
    }
});

