$(document).ready(function () {
     $('#btn').on('click', function() {
        let width = $('#width').val();
        let height = $('#height').val();
        let color = $('#color').val();
        let block = $('<div class=block ></div>').css({
            'background': color,
            'width': parseInt(width) * 10, 
            'height': parseInt(height) * 10
        });
        $('#container').append(block);
        $('.block').on('click', function() {
            $(this).animate({
                top: '10px',
                right: '10px',
                opacity: '50%'
            }, 200, 'linear', function() {
                $(this).animate({
                    left: '10px',
                    bottom: '30px',
                    opacity: '0%'  
                }, 100);
            });
        });
    });


});

