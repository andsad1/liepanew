var reset = document.getElementById('reset');
var submit = document.getElementById('submit');

var loader = document.getElementById('loader');

reset.addEventListener('submit', function(){
    event.preventDefault();
    let password = document.getElementById('password').value;
    let passwordNext = document.getElementById('passwordNext').value;
    document.getElementById('loader').classList.add('loader');
    if (password !==passwordNext) {
        alert ('Passwords are incorrect! Repeat again! ');

    } else {
        loader.style.display = 'block';
        // document.getElementById('loader').classList.add('loader');
        document.getElementById('loader').classList.remove('loader');
        setTimeout(function() {loader.style.display = "none";}, 3000);
        setTimeout (function(){alert("Password changed");}, 4000 );
    };
});