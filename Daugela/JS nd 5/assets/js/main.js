/*******************************************************************************
    
    Created on Mon Aug 19 2020

    JS namų darbas 5 - Paspaudus mygtuką 'Pradiniai duomenys' atspausdinti 
    visas ﬁgūras lentelėje. Paspaudus mygtuką 'Rasti max' suskaičiuoti 
    visų ﬁgūrų tūrius ir rasti ﬁgūrą kurios tūris didžiausias. Atspausdinti visas 
    ﬁgūras lentelėje su suskaičiuotais tūriais, o ﬁgūrą, kurios tūris didžiausias, 
    atspausdinti kitokia spalva.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

var figuros = [
    { ilgis: 5, plotis: 1, aukstis: 8 },
    { ilgis: 2, plotis: 5, aukstis: 3 },
    { ilgis: 3, plotis: 2, aukstis: 6 },
    { ilgis: 1, plotis: 5, aukstis: 5 },
    { ilgis: 1, plotis: 8, aukstis: 5 },
    { ilgis: 4, plotis: 5, aukstis: 1 }
];

var thValues1 = ['ilgis', 'plotis', 'aukštis'];
var thValues2 = ['ilgis', 'plotis', 'aukštis', 'tūris'];
var table1 = document.querySelector('#table1');
var table2 = document.querySelector('#table2');

var maxVolume = 0;

(function captureButtonClicks() {
    var buttons = getButtonsNodeList();
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', processButtonEvent, false);
    }
})();

function getButtonsNodeList() {
    var buttons = document.querySelectorAll('button');
    return buttons;
}

function processButtonEvent() {
    return this.id == 'input' ? createTable(table1) : createTable(table2);
}

function createTable(table) {
    var section = table;
    section.setAttribute('style', 'display: block;');
    section.childElementCount == 2 && section.removeChild(section.lastChild);
    var isVolume = false;
    var tableId = table.id;
    var thValues;
    tableId == 'table1' && (thValues = thValues1);
    tableId == 'table2' && (thValues = thValues2, isVolume = true);
    var table = document.createElement('table');
    var thead = document.createElement('thead');
    var tbody = document.createElement('tbody');
    var tr = document.createElement('tr');

    var row = section.appendChild(table).appendChild(thead).appendChild(tr);
    row.className = 'head';
    thValues.forEach(thValue => {
        var th = document.createElement('th');
        th.append(thValue);
        row.appendChild(th);
    });

    figuros.forEach(figura => {
        figura = Object.assign({}, figura);
        isVolume && (figura.turis = calcVolume(figura));
        tr = document.createElement('tr');
        row = table.appendChild(tbody).appendChild(tr);
        Object.values(figura).forEach(value => {
            var td = document.createElement('td');
            td.append(value);
            row.appendChild(td);
        });
    });
    return setMaxVolumeId();
}

function calcVolume(f) {
    var volume = parseInt(f.ilgis) * parseInt(f.plotis) * parseInt(f.aukstis);
    volume > maxVolume && (maxVolume = volume);
    return volume;
}

function setMaxVolumeId() {
    var rows;
    table2.childElementCount == 2 ? (
         rows = table2.querySelectorAll('tr:not(.head)'),
         rows.forEach(td => {
             td.lastChild.innerText == maxVolume && (td.lastChild.className = "max")
         })
    ) : false;
}
