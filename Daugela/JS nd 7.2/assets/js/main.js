/*******************************************************************************
    
    Created on Mon Aug 24 2020

    JS namų darbas 6 - sukurti 'todo list'.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

var parent = document.querySelector('.parent');
var list = {};
let input = document.querySelector('input');
let button = document.querySelector('button');
button.addEventListener('click', function (e) {
    e.preventDefault();
    getInputObj(e);
});

input.focus();

function getInputObj(e) {
    let task = input.value;
    if (task != '') {
        let id = e.timeStamp;
        list[id] = new InputObj(id, getTime(), task, false);
        input.value = '';
        return setTask(id);
    }
    return;
}

function InputObj(id, time, task, isDone) {
    this.id = id;
    this.time = time;
    this.task = task;
    this.isDone = isDone;
}

function setTask(id) {
    let div = document.createElement('div');
    div.className = 'task';
    div.id = id;
    div.innerHTML = list[id].task;
    div.appendChild(setSpan(list[id].time));
    div.appendChild(setSpan());
    parent.appendChild(div);
    return captureTodoClicks();
}

function captureTodoClicks() {
    var list = document.querySelectorAll('.task');
    for (let i = 0; i < list.length; i++) {
        list[i].addEventListener("click", processTodoEvent);
    }
    return input.focus();
}

function processTodoEvent(e) {
    let id = e.target.id;
    let div = document.getElementById(id);
    if (id == 'close') {
        let cid = e.currentTarget.id;
        let cdiv = document.getElementById(cid);
        cdiv.remove(div);
    } else {
        let task = list[id];
        !task.isDone && (
            task.isDone = true,
            div.classList.add('done'),
            div.innerHTML = list[id].task,
            div.appendChild(setSpan('DONE')),
            div.appendChild(setSpan())
        )
    }
    return input.focus();
}

function setSpan(s) {
    let span = document.createElement('span');
    s != undefined && (span.innerHTML = s);
    s == undefined && (span.id = 'close');
    return span;
}

function getTime() {
    let t = new Date();
    let d = t.toDateString();
    let hrs = t.getHours();
    let min = t.getMinutes();
    let sec = t.getSeconds();
    hrs < 10 && (hrs = '0' + hrs);
    min < 10 && (min = '0' + min);
    sec < 10 && (sec = '0' + sec);
    return d + ', ' + hrs + ':' + min + ':' + sec;
}
