/*******************************************************************************
    
    Created on Sun Aug 30 2020

    JS namų darbas 9 - Praplėsti uždavinį su automobiliais.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

let container = document.querySelector('.container');

let tableHead = document.querySelector('#tableHead');
let buttonNew = document.querySelector('#buttonNew');

let loader = document.querySelector('#loader');
let screen = document.querySelector('#screen');
let form = document.querySelector('form');
let title = document.querySelector('#title');
let inputDate = document.querySelector('#inputDate');
let inputNumber = document.querySelector('#inputNumber');
let inputDriver = document.querySelector('#inputDriver');
let buttonSave = document.querySelector('#buttonSave');
let buttonCancel = document.querySelector('#buttonCancel');
let confirm = document.querySelector('#confirm');
let buttonEdit = document.querySelector('#buttonEdit');
let buttonDelete = document.querySelector('#buttonDelete');

let callerId = null;
let deleteDiv = null;
let date = null;
let number = null;
let driver = null;
let counter = 0;

screen.addEventListener('click', (e) => {
    hideLoader();
    resetInput();
}, false);

buttonNew.addEventListener('click', (e) => {
    e.preventDefault();
    callerId = e.target.id;
    title.innerHTML = "Išsaugoti naują įrašą Nr. " + (counter + 1) + "?";
    showLoader();
    form.style.display == 'none' && (form.style.display = 'flex', confirm.style.display = 'none');
    inputNumber.focus();
}, false);

buttonSave.addEventListener('click', (e) => {
    e.preventDefault();
    callerId == 'buttonNew' && addNewRow(e);
    callerId == 'buttonEdit' && saveTableChanges(e);
    hideLoader();
}, false);

buttonCancel.addEventListener('click', (e) => {
    e.preventDefault();
    resetInput();
    hideLoader();
}, false);

let buttonYes = document.querySelector('#buttonYes');
buttonYes.addEventListener('click', () => {
    resetLoader();
    container.removeChild(deleteDiv);
    title.classList.toggle('red');
}, false);

let buttonNo = document.querySelector('#buttonNo');
buttonNo.addEventListener('click', () => {
    resetLoader();
    title.classList.toggle('red');
}, false);

function addNewRow(e) {
    let div = document.createElement('div');
    div.className = 'table';

    let divNumber = document.createElement('div');
    divNumber.innerHTML = ++counter;
    divNumber.className = 'number';
    div.appendChild(divNumber);

    let divDate = document.createElement('div');
    divDate.innerHTML = inputDate.value;
    divDate.className = 'date';
    div.appendChild(divDate);

    let divCarNumber = document.createElement('div');
    divCarNumber.innerHTML = inputNumber.value;
    divCarNumber.className = 'carNumber';
    div.appendChild(divCarNumber);

    let divDriver = document.createElement('div');
    divDriver.innerHTML = inputDriver.value;
    divDriver.className = 'driver';
    div.appendChild(divDriver);

    let buttonEdit = document.createElement('button');
    buttonEdit.innerHTML = 'Taisyti';
    buttonEdit.id = 'buttonEdit';
    div.appendChild(buttonEdit);
    buttonEdit.addEventListener('click', (e) => {
        e.preventDefault();
        callerId = e.target.id;
        let num = e.target.parentElement.children[0].innerHTML;
        title.innerHTML = "Taisyti įrašą Nr. " + num + "?";
        editTable(e);
    }, false);

    let buttonDelete = document.createElement('button');
    buttonDelete.innerText = 'Naikinti';
    buttonDelete.id = 'buttonDelete';
    div.appendChild(buttonDelete);
    buttonDelete.addEventListener('click', (e) => {
        e.preventDefault();
        deleteDiv = e.target.parentElement;
        confirmDelete(e);
    }, false);

    container.appendChild(div);
    resetInput();
}

function confirmDelete(e) {
    let num = e.target.parentElement.children[0].innerHTML;
    title.innerHTML = 'Ar tikrai norite ištrinti įrašą Nr. ' + num + '?';
    title.classList.toggle('red');
    form.style.display = 'none';
    confirm.style.display = 'flex';
    showLoader();
}

function editTable(e) {
    showLoader();
    form.style.display == 'none' && (form.style.display = 'flex', confirm.style.display = 'none');
    inputNumber.focus();
    let parent = e.target.parentElement;
    date = parent.children[1];
    inputDate.value = parent.children[1].innerHTML;
    number = parent.children[2];
    inputNumber.value = parent.children[2].innerHTML
    driver = parent.children[3];
    inputDriver.value = parent.children[3].innerHTML
}

function saveTableChanges(e) {
    date.innerHTML = inputDate.value;
    number.innerHTML = inputNumber.value;
    driver.innerHTML = inputDriver.value;
    resetInput();
}

function resetInput() {
    inputDate.value = null;
    inputNumber.value = null;
    inputDriver.value = null;
}

function resetLoader() {
    form.style.display = 'flex';
    hideLoader();
    confirm.style.display = 'none';
}

function showLoader() {
    loader.style.display = 'flex';
    screen.style.display = 'block';
    screen.classList.toggle('off');
    loader.classList.toggle('off');
}

function hideLoader() {
    loader.style.display = 'none';
    screen.style.display = 'none';
    screen.classList.toggle('off');
    loader.classList.toggle('off');
}
