/*******************************************************************************
    
    Created on Thu Aug 27 2020

    jQuery namų darbas 1
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

$(document).ready(function () {

    $('#color').click(function () {
        $('select#color').change(
            function () {
                var color = $('option:checked', this).css('background-color');
                $(this).css('background-color', color);
            }
        );
    })

    // $('select#color option').on('hover', function () {      // neveikia
    //     $(this).css('background-color', this.val());
    // })

    $('#set').click(function (e) {
        e.preventDefault();
        var width = $('input#width').val();
        var height = $('input#height').val();
        var color = $('select#color option:checked').val();
        var clr = $('select#color option:checked');

        if (clr[0].id != '') {
            var child = $('<div></div>').css({
                'background-color': color,
                'width': width,
                'height': height
            }).addClass('child').on('click', function () {
                switch (n(5)) {
                    case 0:
                        $(this).animate({
                            width: "0",
                            height: "0",
                            opacity: 0,
                            marginLeft: "50%",
                            marginTop: "50%",
                        }).delay(1500);
                        break;
                    case 1:
                        $(this).animate({
                            width: "0",
                            opacity: 0,
                            marginLeft: "50%",
                        }).delay(1500);
                        break;
                    case 2:
                        $(this).animate({
                            height: "0",
                            opacity: 0,
                            marginTop: "50%",
                        }).delay(1500);
                        break;
                    case 3:
                        $(this).fadeOut();
                        break;
                    case 4:
                        $(this).slideUp();
                        break;
                }
            });

            var childFrame = $('<div></div>').css({
                'width': width,
                'height': height
            }).append(child);

            $('.parent').append(childFrame);

            var n = function (x) {
                return Math.floor(Math.random() * (x));
            }
        }

    });
});
