/*******************************************************************************
    
    Created on Sun Aug 23 2020

    JS namų darbas 5 - šachmatai
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

var gameBoardSim = {};

var events = {
    'white': {
        'king': {
            'id': null,
            'pid' : null,
            'moved': false,
            'check': false,
            'win': false
        },
        'a1': false,
        'h1': false
    },
    'black': {
        'king': {
            'id': null,
            'pid' : null,
            'moved': false,
            'check': false,
            'win': false
        },
        'a8': false,
        'h8': false
    }
}

function setCounter() {
    var game = counter % 2;
    if (game == 0) {
        setPlayer('white');
    } else {
        setPlayer('black');
    }
    counter++;
}

function setPlayer(c) {
    var b = gameBoard;
    Object.entries(b).forEach(e => {
        var f = e[1];
        f.color != c && (
            f.moves = null,
            f.captures = null
        )
    });
}

function setMoved(id) {
    switch (id) {
        case 'E1':
            events.white.king.moved = true
            break;
        case 'E8':
            events.black.king.moved = true
            break;
        case 'A1':
            events.white.a1 = true
            break;
        case 'H1':
            events.white.h1 = true
            break;
        case 'A8':
            events.black.a8 = true
            break;
        case 'H8':
            events.black.h8 = true
            break;
        default:
            break;
    }
}

function getCastle() {
    var b = gameBoard;
    var wKing = 'E1';
    var bKing = 'E8';
    isCastling(b, wKing, events.white.king.moved, events.white.a1, events.white.h1);
    isCastling(b, bKing, events.black.king.moved, events.black.a8, events.black.h8);
}

function isCastling(b, id, king, rook1, rook8) {
    var color = b[id].color;
    var locB = color == 'white' ? 'B1' : 'B8';
    var locC = color == 'white' ? 'C1' : 'C8';
    var locD = color == 'white' ? 'D1' : 'D8';
    var locG = color == 'white' ? 'G1' : 'G8';
    var locF = color == 'white' ? 'F1' : 'F8';
    var isCheck = color == 'white' ? events.white.king.check : events.black.king.check;
    if (!isCheck) {
        if (king == false && rook1 == false) {
            if (isEmptyB(b, locD) && isEmptyB(b, locC) && isEmptyB(b, locB)) {
                b[id].moves == null && (b[id].moves = []);
                b[id].moves.push(locC);
            }
        }
        if (king == false && rook8 == false) {
            if (isEmptyB(b, locG) && isEmptyB(b, locF)) {
                b[id].moves == null && (b[id].moves = []);
                b[id].moves.push(locG);
            }
        }
    }
}

function setCastle(id, cid) {
    var locs = ['C1', 'G1', 'C8', 'G8'];
    var n = locs.includes(cid);
    if (n) {
        var moved = true;
        var b = gameBoard
        if (b[id].color == 'white') {
            moved = events.white.king.moved;
        } else if (b[id].color == 'black') {
            moved = events.black.king.moved;
        }
        if (!moved) {
            var rookId, locId;
            switch (cid) {
                case 'C1':
                    rookId = 'A1';
                    locId = 'D1';
                    break;
                case 'G1':
                    rookId = 'H1';
                    locId = 'F1';
                    break;
                case 'C8':
                    rookId = 'A8';
                    locId = 'D8';
                    break;
                case 'G8':
                    rookId = 'H8';
                    locId = 'F8';
                    break;
            }
            var rook = document.getElementById(rookId).firstChild;
            document.getElementById(locId).appendChild(rook);
        }
    }
}

function newQueen(id, cid) {
    var b = gameBoard;
    var y = getCoordinates(id).row;
    var color = b[id].color;
    switch (color) {
        case 'white':
            if (y == 7) {
                dragFigure.className = 'white queen';
                dragFigure.innerHTML = '♕';
            }
            break;
        case 'black':
            if (y == 2) {
                dragFigure.className = 'black queen';
                dragFigure.innerHTML = '♕';
            }
            break;
    }
}

function isCheck(id, cid) {
    var safeToMove = true;
    var b = gameBoard;
    var color = b[id].color;
    var king, captureKing, check;
    switch (color) {
        case 'white':
            king = events.white.king.id;
            captureKing = events.black.king.id;
            check = events.white.king.check;
            break;
        case 'black':
            king = events.black.king.id;
            captureKing = events.white.king.id;
            check = events.black.king.check;
            break;
    }
    if (cid == captureKing) {
        safeToMove = false
    } else if (id == king) {
        Object.entries(b).forEach(e => {
            var f = e[1];
            if (f.moves != null && color != f.color) {
                f.moves.forEach(move => {
                    if (cid == move) {
                        safeToMove = false;
                    }
                });
            }
        });
        gameBoardSim = JSON.parse(JSON.stringify(gameBoard));
        var b = gameBoardSim;
        reset(id, cid);
        capturePositions(b);
        getAttacks(b);
        Object.entries(b).forEach(e => {
            var f = e[1];
            if (f.captures != null) {
                f.captures.forEach(capture => {
                    if (capture == cid) {
                        safeToMove = false;
                    }
                });
            }
        });
    } else {
        gameBoardSim = JSON.parse(JSON.stringify(gameBoard));
        var b = gameBoardSim;
        reset(id, cid);
        capturePositions(b);
        getAttacks(b);
        Object.entries(b).forEach(e => {
            var f = e[1];
            if (f.captures != null) {
                f.captures.forEach(capture => {
                    if (capture == king) {
                        safeToMove = false;
                    }
                });
            }
        });
    }
    return safeToMove;
}

function getAttacks(boardName) {
    var b = boardName;
    Object.entries(b).forEach(e => {
        var f = e[1];
        if (f.captures != null) {
            f.captures.forEach(capture => {
                b[capture].underAttack = true;
            });
        }
    });
    setCheck();
}

function setCheck() {
    events.white.king.check = gameBoard[events.white.king.id].underAttack;
    events.black.king.check = gameBoard[events.black.king.id].underAttack;
}

function reset(id, cid) {
    var b = gameBoardSim;
    var from = b[id];
    var name = from.name;
    var color = from.color;

    Object.entries(b).forEach(e => {
        var f = e[1];
        f.moves = null;
        f.captures = null;
        f.underAttack = false;
    });

    var to = b[cid];
    to.name = name;
    to.color = color;
    from.name = null;
    from.color = null;
}

function resetAll() {
    var b = gameBoardSim;
    Object.entries(b).forEach(e => {
        var f = e[1];
        f.moves = null;
        f.captures = null;
    });
}

function removeCapturedFigure(cid) {
    var capturedFigure = document.getElementById(cid).firstChild;
    return capturedFigure.remove();
}

function check() {
    var b = gameBoard;
    var whiteArr = [];
    var blackArr = [];
    var mainArr = [];
    Object.entries(b).forEach(e => {
        var id = e[0];
        var f = e[1];
        var color = f.color;
        switch (color) {
            case 'white':
                mainArr = whiteArr;
                break;
            case 'black':
                mainArr = blackArr;
                break;
        }
        if (f.moves != null) {
            var movesArr = [];
            f.moves.forEach(move => {
                isCheck(id, move) == true && movesArr.push(move);
            });
            b[id].moves = movesArr;
            mainArr.push(movesArr);
        }
        if (f.captures != null) {
            var capturesArr = [];
            f.captures.forEach(capture => {
                isCheck(id, capture) == true && capturesArr.push(capture);
            });
            b[id].captures = capturesArr;
            mainArr.push(capturesArr);
        }
    });
    isArrayEmpty(whiteArr) && (
        events.black.king.win = true,
        log(),
        alert('Black wins!'));
    isArrayEmpty(blackArr) && (
        events.white.king.win = true,
        log(),
        alert('White wins!'),
        events.white.king.win = true);
    setCheck();
}

function log() {
    var txt = document.getElementById('text');
    txt.innerHTML = ' ';
    if (events.white.king.win || events.black.king.win) {
        events.white.king.win && (txt.innerHTML += 'White wins!');
        events.black.king.win && (txt.innerHTML += 'Black wins!');
    } else {
        events.white.king.check && (txt.innerHTML += 'Black is checking White!');
        events.black.king.check && (txt.innerHTML += 'White is checking Black!');
    }
}

function isArrayEmpty(arr) {
    var empty = true;
    arr.forEach(e => {
        e.length != 0 && (empty = false);
    });
    empty && gameOver();
    return empty;
}

function gameOver() {
    var b = gameBoard;
    Object.entries(b).forEach(e => {
        var f = e[1];
        f.moves = null;
        f.captures = null;
    });
}

function showCheck() {
    var whitePid = events.white.king.pid;
    var blackPid = events.black.king.pid;
    if (events.white.king.check) {
        document.getElementById(events.white.king.id).classList.add('red');
        events.white.king.pid = events.white.king.id;
    } else {
        document.getElementById(events.white.king.id).classList.remove('red');
        events.white.king.pid != null && (document.getElementById(whitePid).classList.remove('red'),
        events.white.king.pid = null);
    }
    if (events.black.king.check) {
        document.getElementById(events.black.king.id).classList.add('red');
        events.black.king.pid = events.black.king.id;
    } else {
        document.getElementById(events.black.king.id).classList.remove('red');
        events.black.king.pid != null && (document.getElementById(blackPid).classList.remove('red'),
        events.black.king.pid = null);
    }
}

function showPossibleMoves(x) {
    var id = x.id;
    id == undefined && (id = x);
    var b = gameBoard;
    var moves = b[id].moves 
    moves != null && (
        moves.lenth != 0 && document.getElementById(id).classList.add('green'),
        moves.forEach(move => {
            document.getElementById(move).classList.add('green');
        })
    )
    var captures = b[id].captures 
    captures != null && (
        captures.forEach(capture => {
            document.getElementById(capture).classList.add('red');
        })
    )
}

function removePossibleMoves(x) {
    var id = x.id;
    id == undefined && (id = x);
    var b = gameBoard;
    var moves = b[id].moves
    moves != null && moves.lenth != 0 && (
        document.getElementById(id).classList.remove('green'),
        moves.forEach(move => {
            document.getElementById(move).classList.remove('green');
        })
    )
    var captures = b[id].captures 
    captures != null && (
        captures.forEach(capture => {
            document.getElementById(capture).classList.remove('red');
        })
    )
}
