/*******************************************************************************
    
    Created on Sun Aug 23 2020

    JS namų darbas 5 - šachmatai
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

var Figures = {
    'rook': {               // bokštas
        'horzMove': '7',
        'vertMove': '7',
        'diagMove': '0',
        'horseMove': '0'
    },
    'knight': {             // žirgas
        'horzMove': '0',
        'vertMove': '0',
        'diagMove': '0',
        'horseMove': '1'
    },
    'bishop': {             // rikis
        'horzMove': '0',
        'vertMove': '0',
        'diagMove': '7',
        'horseMove': '0'
    },
    'queen': {              // valdovė
        'horzMove': '7',
        'vertMove': '7',
        'diagMove': '7',
        'horseMove': '0'
    },
    'king': {               // karalius
        'horzMove': '1',
        'vertMove': '1',
        'diagMove': '1',
        'horseMove': '0'
    },
    'pawn': {               // pėstininkas
        'horzMove': '0',
        'vertMove': '1',
        'diagMove': '0',
        'horseMove': '0'
    }
}

function getMoves(boardName, id) {
    var b = boardName;
    var name = b[id].name;
    var color = b[id].color;
    var col = parseInt(getCoordinates(id).col);
    var row = parseInt(getCoordinates(id).row);
    var x = col;
    var y = row;

    var figure = Figures[name];
    var horizontal = parseInt(figure.horzMove);
    var vertical = parseInt(figure.vertMove);
    var diagonal = parseInt(figure.diagMove);
    var horse = parseInt(figure.horseMove);

    var array = [];
    var lid;

    if (horizontal) {
        for (let i = 0; i < horizontal; i++) {
            x >= 7 ? i = horizontal : (
                lid = rowLetters[++x] + y,
                isEmpty(b, id, lid, color) ? array.push(lid) : i = horizontal
            );
        }
        x = col;
        for (let i = 0; i < horizontal; i++) {
            x == 0 ? i = horizontal : (
                lid = rowLetters[--x] + y,
                isEmpty(b, id, lid, color) ? array.push(lid) : i = horizontal
            );
        }
    }
    if (vertical) {
        x = col;
        if (name == 'pawn' && color == 'white' || name != 'pawn') {
            for (let i = 0; i < vertical; i++) {
                y >= 8 ? i = vertical : (
                    lid = rowLetters[x] + ++y,
                    isEmpty(b, id, lid, color) ? array.push(lid) : i = vertical
                );
            }
        }
        y = row;
        if (name == 'pawn' && color == 'black' || name != 'pawn') {
            for (let i = 0; i < vertical; i++) {
                y == 1 ? i = vertical : (
                    lid = rowLetters[x] + --y,
                    isEmpty(b, id, lid, color) ? array.push(lid) : i = vertical
                );
            }
        }
    }
    if (diagonal) {
        y = row;
        x = col;
        for (let i = 0; i < diagonal; ++i) {
            x == 7 || y == 8 ? i = diagonal : (
                lid = rowLetters[++x] + ++y,
                isEmpty(b, id, lid, color) ? array.push(lid) : i = diagonal
            );
        }
        y = row;
        x = col;
        for (let i = 0; i < diagonal; ++i) {
            x == 7 || y == 1 ? i = diagonal : (
                lid = rowLetters[++x] + --y,
                isEmpty(b, id, lid, color) ? array.push(lid) : i = diagonal
            );
        }
        y = row;
        x = col;
        for (let i = 0; i < diagonal; ++i) {
            x == 0 || y == 1 ? i = diagonal : (
                lid = rowLetters[--x] + --y,
                isEmpty(b, id, lid, color) ? array.push(lid) : i = diagonal
            );
        }
        y = row;
        x = col;
        for (let i = 0; i < diagonal; ++i) {
            x == 0 || y == 8 ? i = diagonal : (
                lid = rowLetters[--x] + ++y,
                isEmpty(b, id, lid, color) ? array.push(lid) : i = diagonal
            );
        }
    }
    if (horse) {
        var x1 = x + 1;
        var x2 = x - 1;
        var y1 = y + 2;
        var y2 = y - 2;
        if (x1 <= 7) {
            if (y1 <= 8) {
                lid = rowLetters[x1] + y1;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
            if (y2 > 0) {
                lid = rowLetters[x1] + y2;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
        }
        if (x2 >= 0) {
            if (y1 <= 8) {
                lid = rowLetters[x2] + y1;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
            if (y2 > 0) {
                lid = rowLetters[x2] + y2;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
        }
        var x1 = x + 2;
        var x2 = x - 2;
        var y1 = y + 1;
        var y2 = y - 1;
        if (x1 <= 7) {
            if (y1 <= 8) {
                lid = rowLetters[x1] + y1;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
            if (y2 > 0) {
                lid = rowLetters[x1] + y2;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
        }
        if (x2 >= 0) {
            if (y1 <= 8) {
                lid = rowLetters[x2] + y1;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
            if (y2 > 0) {
                lid = rowLetters[x2] + y2;
                isEmpty(b, id, lid, color) && array.push(lid);
            }
        }
    }
    x = col;
    y = row;
    if (name == 'pawn' && color == 'white') {
        b[id].captures != null && (b[id].captures = null);
        var lid = rowLetters[x + 1] + (y + 1);
        if (lid) {
            isEmpty(b, id, lid, color);
        }
        var lid = rowLetters[x - 1] + (y + 1);
        if (lid) {
            isEmpty(b, id, lid, color);
        }
        if (y == 2) {
            y += 1;
            lid = rowLetters[x] + y;
            if (isEmptyB(b, lid)) {
                y = 2;
                y += 2;
                lid = rowLetters[x] + y;
                isEmptyB(b, lid) && array.push(lid);
            }
        }
    }
    if (name == 'pawn' && color == 'black') {
        b[id].captures != null && (b[id].captures = null);
        var lid = rowLetters[x + 1] + (y - 1);
        if (lid) {
            isEmpty(b, id, lid, color);
        }
        var lid = rowLetters[x - 1] + (y - 1);
        if (lid) {
            isEmpty(b, id, lid, color);
        }
        if (y == 7) {
            y -= 1;
            lid = rowLetters[x] + y;
            if (isEmptyB(b, lid)) {
                y = 7;
                y -= 2;
                lid = rowLetters[x] + y;
                isEmptyB(b, lid) && array.push(lid);
            }
        }
    }
    return array;
}

function isEmpty(boardName, id, lid, color) {
    var b = boardName;
    var field1 = b[id];
    var field2 = b[lid];
    var array = [];
    var empty;
    if (field2 == undefined) {
        empty = false;
    } else {
        var empty = field2.name == null ? true : false;
        if (!empty && field2.color != color) {
            var arr = field1.captures;
            arr != null && (
                arr.forEach(e => {
                    e == lid && (arr = null);
                })
            );
            arr != null && (array = arr);
            array.push(lid);
            field1.captures = array;
        }
    }
    return empty;
}

function isEmptyB(boardName, lid) {
    var b = boardName;
    var field2 = b[lid];
    var empty = field2.name == null ? true : false;
    return empty;
}

