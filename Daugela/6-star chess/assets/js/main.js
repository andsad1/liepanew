/*******************************************************************************
    
    Created on Sun Aug 23 2020

    JS namų darbas 5 - šachmatai
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

var dragFigure;
var rowLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
var gameBoard = {};
var counter = 0;

function getDesk() {
    var deskNode = getDeskNodeList();
    gameBoard = {};
    var n = 0;
    for (var i = 8; i > 0; i--) {
        for (var j = 0; j < 8; j++) {
            var fieldName = setCoordinates(j, i);
            var d = deskNode[n++].firstChild;
            var className, str, name, color;
            className = d != null ? d.className : null;
            gameBoard[fieldName] = className != null ? (
                str = className.split(' '),
                name = str[1],
                color = str[0],
                name == 'king' && (events[color].king.id = fieldName),
                new Figure(str[1], str[0], null, null, false)
            ) : new Figure(null, null, null, null, false);
        }
    }
    capturePositions(gameBoard);
    getAttacks(gameBoard);
    getCastle();
    check();
    showCheck();
    setCounter();
    console.clear();
    console.log('White is checking: ' + events.black.king.check);
    console.log('Black is checking: ' + events.white.king.check);
    console.table(gameBoard, ["name", "color", "moves", "captures"]);
    log();
}

function setCoordinates(j, i) {
    return rowLetters[j] + i;
}

function getCoordinates(id) {
    var x = id.charAt(0);
    var y = id.charAt(1);
    rowLetters.forEach((e, i) => {
        e == x && (x = i);
    });
    return { 'col': x, 'row': y };
}

function capturePositions(deskname) {
    for (var [id, figure] of Object.entries(deskname)) {
        var name = figure.name;
        var color = figure.color;
        name != null && (figure.moves = getMoves(deskname, id));
    }
}

function Figure(name, color, moves, captures, underAttack) {
    this.name = name;
    this.color = color;
    this.moves = moves;
    this.captures = captures;
    this.underAttack = underAttack;
}

function getDeskNodeList() {
    var deskNode = document.querySelectorAll('.box');
    return deskNode;
}

(function captureFigureDragstart() {
    var figures = getFiguresNodeList();
    for (var i = 0; i < figures.length; i++) {
        figures[i].addEventListener('dragstart', dragstartHandler, false);
    }
})();

function getFiguresNodeList() {
    var figures = document.querySelectorAll('.black, .white');
    return figures;
}

function dragstartHandler(event) {
    dragFigure = event.target;
    event.dataTransfer.setData('text/plain', event.target.className);
}

function dragoverHandler(event) {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
}
function dropHandler(event) {
    event.preventDefault();
    var cid = event.target.id;
    if (cid == '') {
        cid = event.target.parentElement.id;
    }
    if (dragFigure == undefined) {
        return;
    } else {
        var id = dragFigure.parentElement.id;
        isAllowed(id, cid) && (
            dragFigure != null && event.currentTarget.appendChild(dragFigure),
            removePossibleMoves(id),
            setMoved(id),
            getDesk()
        );
    }
    dragFigure = null;
}

function isAllowed(id, cid) {
    if (id == undefined || id == '') {
        return false;
    }
    var allowed = false;
    var b = gameBoard;
    var moves = b[id].moves;
    moves != null && (
        moves.forEach(e => {
            e == cid && (allowed = isCheck(id, cid),
            (allowed && b[id].name == 'king') && setCastle(id, cid)),
            allowed && b[id].name == 'pawn' && newQueen(id, cid);
        })
    );
    var captures = b[id].captures;
    captures != null && (
        captures.forEach(e => {
            e == cid && (allowed = isCheck(id, cid),
                allowed && removeCapturedFigure(cid)),
                allowed && b[id].name == 'pawn' && newQueen(id, cid);
        })
    );
    return allowed;
}

(function () {
    return getDesk();
})();
