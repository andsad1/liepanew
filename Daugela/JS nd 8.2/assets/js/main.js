/*******************************************************************************
    
    Created on Tue Aug 25 2020

    JS namų darbas 8.2 - 'lenktynes'.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

let parent = document.querySelector('.parent');
var carList = [];
let carPic = ['car1.png', 'car2.png', 'car3.png', 'car4.png', 'car5.png', 'car6.png', 'car7.png', 'car8.png', 'car9.png', 'car10.png', 'car11.png', 'car12.png'];
let carNameList = ['Alfa Romeo Spider', 'Alpine A310', 'Aston Martin DB7 Vantage', 'Audi e-tron GT', 'BMW i8', 'Bugatti Chiron', 'Ferrari 860 Monza', 'Honda S2000', 'Lamborghini Diablo VT', 'Maserati GranSport', 'McLaren 570S GT4', 'Mercedes-AMG S 65', 'Porsche 911 Speedster', 'Tesla Roadster', 'Tesla Model S', 'Volkswagen Scirocco R']
let carPicUrl = 'assets/img/';
let carName = document.querySelector('#name');
let carSpeed = document.querySelector('#speed');
let raceTime = document.querySelector('#time');
carName.focus();

function setRandomCar(e) {
    let name = carNameList[getRandomNumber(carNameList.length)];
    let speed = getRandomNumber(250);
    let img = setPicture();
    let id = e.timeStamp;
    let car = new Car(id, img, name, speed);
    carList.push(car);
    run(car, e);
    return reset();
}

function setCar(e) {
    let name = carName.value;
    let speed = carSpeed.value;
    if (name != '' && speed != '') {
        let img = setPicture();
        let id = e.timeStamp;
        let car = new Car(id, img, name, isNumber(speed));
        carList.push(car);
        run(car, e);
        return reset();
    }
    return;
}

function Car(id, img, name, speed) {
    this.id = id;
    this.img = img;
    this.name = name;
    this.speed = speed;
    this.distance = (time) => {
        return Math.round(time / 60 * this.speed);
    }
    this.setDistance = (distance) => {
        this.distance = distance;
    }
}

function run(car, e) {
    if (parent.firstChild != null && parent.firstChild.className == 'race') {
        removeDiv('race');
        carList.splice(0, carList.length);
        carList.push(car);
        return getCar(car);
    }
    return getCar(car);
}

function getCar(car) {
    let div = document.createElement('div');
    div.classList.add('data');
    setInnerHtml('data', div, car);
    parent.appendChild(div);
}

function startRace() {
    if (isNumber(raceTime.value) != 0) {
        if (parent.firstChild != null && parent.firstChild.className == 'data') {
            removeDiv('data');
            carList.forEach(car => {
                let div = document.createElement('div');
                div.classList.add('race');
                setInnerHtml('race', div, car);
                parent.appendChild(div);
            });
        }
        maxDistance();
        reset();
    }
    return;
}

function setInnerHtml(event, div, car) {
    switch (event) {
        case 'data':
            div.appendChild(getSpan());
            div.appendChild(getPicture(car.img));
            div.appendChild(getSpan(car.name + ' [' + car.speed + ' km/h]'));
            break;
        case 'race':
            let distance = car.distance(raceTime.value);
            car.setDistance(distance);
            let span = getSpan();
            span.id = car.id;
            // span.style.width = 140 + distance + 'px';
            span.innerHTML = distance + ' km';
            div.appendChild(span);
            div.appendChild(getPicture(car.img));
            div.appendChild(getSpan(car.name));
            break;
    }
}

function getSpan(s) {
    let span = document.createElement('span');
    s == undefined && (span.className = 'distance');
    s != undefined && (span.innerHTML = s, span.className = 'value');
    return span;
}

function removeDiv(className) {
    let divs = document.getElementsByClassName(className);
    let length = divs.length;
    for (let i = 0; i < length; i++) {
        parent.removeChild(divs[0]);
    }
}

function getPicture(i) {
    let img = document.createElement('img');
    let src = carPicUrl + i;
    let alt = 'The racing ' + i;
    img.setAttribute("src", src);
    img.setAttribute("width", "135px");
    img.setAttribute("height", "60px");
    img.setAttribute("alt", alt);
    return img;
}

function setPicture() {
    return carPic[getRandomNumber(carPic.length + 1)];
}

function getRandomNumber(n) {
    return Math.floor(Math.random() * (n - 1));
}

function maxDistance() {
    var max = 0;
    carList.forEach(car => {
        let carDistance = car.distance;
        carDistance > max && (max = carDistance);
    });
    carList.forEach(car => {
        car.distance == max && document.getElementById(car.id).classList.add('max');
    });
    setSpanWidth(max);
}

function setSpanWidth(max) {
    let w = window.innerWidth - 870;
    carList.forEach(car => {
        let k;
        let id = car.id;
        let carDistance = car.distance;
        carDistance == max && (k = 1);
        carDistance < max && (k = carDistance / max);
        document.getElementById(id).style.width = 140 + w * k + 'px';
    });
}

function reset() {
    carName.value = '';
    carSpeed.value = '';
    raceTime.value = '';
    carName.focus();
}

function isNumber(n) {
    return isNaN(n) || n == "" ? 0 : parseInt(n);
}

let set = document.querySelector('#set');
set.addEventListener('click', function (e) {
    e.preventDefault();
    setCar(e);
});

let setRandom = document.querySelector('#setRandom');
setRandom.addEventListener('click', function (e) {
    e.preventDefault();
    setRandomCar(e);
    return;
});

let race = document.querySelector('#race');
race.addEventListener('click', function (e) {
    e.preventDefault();
    startRace(e);
});
