/*******************************************************************************
    
    Created on Mon Aug 17 2020

    JS namų darbas 4 - išvesti visus automobilius į html lentelę, kartu su 
    suskaičiuotu automobilio greičiu kilometrais per valandą.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

(function captureButtonClicks() {
    var button = document.querySelector('button');
    button.addEventListener('click', function () {
        return insertNewRow();
    });
})();

function getInputNodeList() {
    var input = document.querySelectorAll('input');
    return input;
}

function getInputObj() {
    var input = getInputNodeList();
    var date = isDate(input[0].value) + ' ' + isTime(input[1].value);
    var carNumber = isCarNumber(input[2].value);
    var distance = convertToNumber(input[3].value);
    var time = convertToNumber(input[4].value);
    resetInputValues(input);
    return new InputObj(date, carNumber, distance, time);
}

function InputObj(date, carNumber, distance, time) {
    this.date = date;
    this.carNumber = carNumber;
    this.distance = distance;
    this.time = time;
}

function resetInputValues(input) {
    input[0].value = '2020-07-07';
    input[1].value = '07:30';
    input[2].value = '';
    input[2].focus();
    input[3].value = '';
    input[4].value = '';
}

function convertToNumber(n) {
    return isNaN(n) || n == "" || n == Infinity ? 0 : parseFloat(n);
}

function isTime(n) { return n == "" ? '07:30' : n; }

function isDate(n) { return n == "" ? '2020-07-07' : n; }

function isCarNumber(n) { return n == "" ? 'AAA000' : n; }

function getTableNode() {
    var table = document.querySelector('tbody');
    return table;
}

function getRowsNodeList() {
    var rows = document.querySelectorAll('tbody tr');
    return rows;
}

function captureTableClicks() {
    var rows = getRowsNodeList();
    for (let i = 0; i < rows.length; i++) {
        rows[i].addEventListener("click", processTableEvent, false);
    }
}

function insertNewRow() {
    var input = getInputObj();

    var row = getTableNode().insertRow(-1);
    row.id = 'row-' + row.rowIndex;

    var i = 0;
    var cell = row.insertCell(i++);
    cell.innerHTML = input.date;
    cell.className = 'date';

    cell = row.insertCell(i++);
    cell.innerHTML = input.carNumber;
    cell.className = 'carNumber';

    cell = row.insertCell(i++);
    cell.innerHTML = input.distance;
    cell.className = 'distance';

    cell = row.insertCell(i++);
    cell.innerHTML = input.time;
    cell.className = 'time';

    cell = row.insertCell(i++);
    cell.innerHTML = calcSpeed(input.distance, input.time);
    cell.className = 'speed';

    cell = row.insertCell(i);
    cell.innerHTML = "ištrinti";
    cell.className = 'delete';

    // return resetInputValues();
    return captureTableClicks();
}

function calcSpeed(distance, time) {
    var speed = (distance / 1000) / (time / 3600);
    speed = convertToNumber(speed);
    return Math.round(speed * 100) / 100 + ' km/h';
}

function processTableEvent(event) {
    var rowId = event.currentTarget.id;
    var cellClass = event.target.className;
    if (cellClass == 'delete') {
        var rowIndex = event.currentTarget.rowIndex - 1;
        confirm("ar ištrinti?") && getTableNode().deleteRow(rowIndex);
    } else {
        var cellValue = event.target.innerHTML;
        var newValue = cellClass !== 'speed' ? newValue = prompt('\nįrašyti pakeitimą - [OK]\natmesti - [Cancel]\n\n', cellValue) : null;
        if (newValue != null) {
            switch (cellClass) {
                case 'date':
                    newValue = isDate(newValue);
                    break;
                case 'carNumber':
                    newValue = isCarNumber(newValue);
                    break;
                case 'distance':
                case 'time':
                    newValue = convertToNumber(newValue);
                    break;
            }
            return newValue != cellValue && updateCell(rowId, cellClass, newValue);
        }
    }
    return false;
}

function updateCell(rowId, cellClass, newValue) {
    var row = '#' + rowId;
    var cell = ' .' + cellClass;
    document.querySelector(row + cell).innerHTML = newValue;
    if (cellClass == 'distance' || 'time') {
        var distance = document.querySelector(row + ' .distance').innerHTML;
        var time = document.querySelector(row + ' .time').innerHTML;
        document.querySelector(row + ' .speed').innerHTML = calcSpeed(distance, time);
    }
}
