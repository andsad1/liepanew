/*******************************************************************************
    
    Created on Thu Aug 27 2020

    JS namų darbas 8.2 - 'quiz 1.1'.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

let quizBoard = document.querySelector('#quizBoard');
let quizSelect = document.querySelector('#quizSelect');
let loader = document.querySelector('#loader');
let label = document.querySelectorAll('label');
let divC = document.querySelector('#count');
let divQ = document.querySelector('#question');
let divB = document.querySelector('#buttons');
let divR = document.querySelector('#result');
let btnT = document.querySelector('#True');
let btnF = document.querySelector('#False');

let categories = {
    'any': 'Any Category',
    '9': 'General Knowledge',
    '10': 'Entertainment: Books',
    '11': 'Entertainment: Film',
    '12': 'Entertainment: Music',
    '13': 'Entertainment: Musicals & Theatres',
    '14': 'Entertainment: Television',
    '15': 'Entertainment: Video Games',
    '16': 'Entertainment: Board Games',
    '17': 'Science & Nature',
    '18': 'Science: Computers',
    '19': 'Science: Mathematics',
    '20': 'Mythology',
    '21': 'Sports',
    '22': 'Geography',
    '23': 'History',
    '24': 'Politics',
    '25': 'Art',
    '26': 'Celebrities',
    '27': 'Animals',
    '28': 'Vehicles',
    '29': 'Entertainment: Comics',
    '30': 'Science: Gadgets',
    '31': 'Entertainment: Japanese Anime & Manga',
    '32': 'Entertainment: Cartoon & Animations',
}

var qArr = [];      // arrray of 'questions & answers'
var quiz = null;    // quiz obj
var c = 0;          // counter of questions

let btnSet = document.querySelector('#set');
btnSet.addEventListener('click', function (e) {
    start();
});

function getJSON() {
    loader.style.display = 'block';
    var request = new XMLHttpRequest();
    request.responseType = 'json';
    request.open('GET', quiz.url, true);
    request.onload = function () {
        return captureData(request.response);
    };
    request.send();
}

function captureData(data) {
    let a = data.results;
    let isRepeting = false;
    let isFound = false;
    if (a.length == 0) {
        let empty = 'category "' + quiz.category + '" and dificulty "' + quiz.difficulty + '"';
        showResults(empty);
    } else {
        a.forEach(a => {
            qArr.forEach(q => {
                q.question == a.question && (isRepeting = true);
            });
            if (!isRepeting) {
                let question = a.question;
                let answer = a.correct_answer;
                let q = new Q(question, answer, null);
                qArr.push(q);
                isFound = true;
            }
        });
        isFound && getQuestion();
        !isFound && getJSON();
    }
}

function getQuestion() {
    if (qArr.length != 0 && qArr.length >= c) {
        loader.style.display = 'none';
        divC.innerHTML = c + 1;
        divQ.innerHTML = qArr[c].question;
        divB.style.display = 'flex';
        // console.log(qArr[c]);
    } else {
        getJSON()
    }
}

function getAnswer(e) {
    qArr[c].selectedAnswer = e.target.id;
    ++c < 10 && getQuestion();
    c == 10 && showResults();
}

function showResults(e) {
    if (e == undefined) {
        let n = 0;
        qArr.forEach(a => {
            a.correctAnswer == a.selectedAnswer && n++;
        });
        divC.innerHTML = '';
        divB.style.display = "none";
        divQ.innerHTML = '<p>Correct answers:</p>';
        divR.innerHTML = '<p>' + n + '</p>';
    } else {
        loader.style.display = 'none';
        divQ.innerHTML = '<p>too few topics for <span>' + e + '</span>, try another options</p>';
    }
    quizSelect.style.display = 'flex';
    qArr.splice(0, qArr.length);        // empty array
    Object.keys(quiz).forEach(k => delete quiz[k])      // empty object
    disable(false);
}

function start() {
    let cat = document.querySelector('#category');
    let category = cat.value;
    let difficulty = document.querySelector('#difficulty').value;
    let type = document.querySelector('#type').value;
    let url = generateUrl(category, difficulty, type);
    quiz = new Quiz(url, categories[category], difficulty, type);
    // console.clear();
    // console.log(categories[category]);
    // console.log(difficulty);
    // console.log(type);
    disable(true);
    divR.innerHTML = '';
    divQ.innerHTML = '';
    c = 0;
    getJSON();
}

function Quiz(url, category, difficulty, type) {
    this.url = url;
    this.category = category;
    this.difficulty = difficulty;
    this.type = type;
}

function Q(question, correctAnswer, selectedAnswer) {
    this.question = question;
    this.correctAnswer = correctAnswer;
    this.selectedAnswer = selectedAnswer;
}

function generateUrl(cat, dif, typ) {
    let c = cat != undefined && cat != 'any' ? '&category=' + cat : '';
    let d = dif != undefined && dif != 'any' ? '&difficulty=' + dif : '';
    let t = typ != undefined && typ != 'any' ? '&type=' + typ : '';
    return 'https://opentdb.com/api.php?amount=10' + c + d + t;
}

function reset() {
    divC.innerHTML = '';
    divQ.innerHTML = '';
    divB.style.display = "none";
}

function disable(e) {
    category.disabled = e;
    difficulty.disabled = e;
    type.disabled = e;
    btnSet.disabled = e;
    e == true && (label.forEach(l => { l.style.opacity = .4 }));
    e == false && (label.forEach(l => { l.style.opacity = 1 }));
}

btnT.addEventListener('click', function (e) {
    e.preventDefault();
    reset();
    getAnswer(e);
});

btnF.addEventListener('click', function (e) {
    e.preventDefault();
    reset();
    getAnswer(e);
});
