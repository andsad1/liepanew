/*******************************************************************************
    
    Created on Tue Aug 25 2020

    JS namų darbas 8.2 - 'slaptažodžio keitimas'.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

let pass1 = document.querySelector('#ps1');
let pass2 = document.querySelector('#ps2');
let main = document.querySelector('#main');
let loader =  document.querySelector('#loader');
pass1.focus();

function changePassword() {
    if (ps1.value != '' && ps2.value != '') {
        if (ps1.value === ps2.value) {
            setTimeout(changed, 2400);
            main.style.opacity='0.4';
            loader.style.display = "block";
        } else {
            setTimeout(notChanged, 2400);
            main.style.opacity='0.4';
            loader.style.display = "block";
        }
    } else return
}

function changed() {
    loader.style.display = "none";
    main.style.opacity='1';
    reset();
    alert('slaptažodis sėkmingai atnaujintas');
}

function notChanged() {
    loader.style.display = "none";
    main.style.opacity='1';
    reset();
    pass1.focus();
    alert('slaptažodžiai nesutampa, pakartoti');
}

function reset() {
    pass1.value = '';
    pass2.value = '';
}

let set = document.querySelector('#set');
set.addEventListener('click', function (e) {
    e.preventDefault();
    changePassword(e);
});