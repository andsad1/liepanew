/*******************************************************************************
    
    Created on Mon Aug 24 2020

    JS namų darbas 6 - Sukurti HTML su raudonu kvadratu. Padarykite taip, kad 
    paspaudus ant to kvadrato jis per vieną sekundę pakeistų spalvą į žalią ir 
    taptų apskritimu. Antrą kartą paspaudus vėl taptų raudonu kvadratu ir t.t.
    
    Copyright (C) 2020 Tomas Daugėla

    Home: https://bitbucket.org/tdaugela/
    
*/

'use strict';

var figure = document.createElement('div');
figure.addEventListener('click', changeForm);
document.querySelector('main').appendChild(figure);

var set = figure.style;
var counter = 0;

set.height = '100%';
set.width = '100%';

function changeForm() {
    if (counter++ % 2 != 0) {
        set.backgroundColor = 'green';
        set.borderRadius = '100%';
        set.transitionTimingFunction = 'ease-in';
        set.transitionDuration = "1s"
    } else {
        set.backgroundColor = 'red';
        set.borderRadius = '0%';
        set.transitionTimingFunction = 'ease-in-out';
        set.transitionDuration = "1s"
    }
}

(() => changeForm())();
