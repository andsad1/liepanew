function cars (cn, cs) {
    this.name = cn;
    this.speed = cs;
}

var info = document.getElementById('car_info');
var list = document.getElementById('carsList');
var m = [];

info.addEventListener('submit', function(e) {
    e.preventDefault();
    
    var name = document.getElementById('car_name').value; //assigning data from inputs
    var speed = document.getElementById('car_speed').value; //up
    var carConstructor = new cars(name, speed);     //making Cars object with cars constructor
    m.push(carConstructor);                         //pushing cars to array m
    var carDiv = document.createElement('div');     //making car name div
    carDiv.innerHTML = name;                        //writing typed car names inside the new div
    var speedDiv = document.createElement('div');   //making speed div
    speedDiv.innerHTML = speed;                     //writing typed speed data inside the new div
    var dataDiv = document.createElement('div');    //making data parrent div 
    dataDiv.classList.add('data');                  //assigning new class for that
    dataDiv.appendChild(carDiv);
    dataDiv.appendChild(speedDiv);
    list.appendChild(dataDiv);
    document.getElementById('car_name').value = "";
    document.getElementById('car_speed').value = "";
    console.log(speedDiv)
});

var racing = document.getElementById('car_racing');
var results = document.getElementById('results');

racing.addEventListener('submit', function(e) {
    e.preventDefault();
    var time = document.getElementById('time').value;
    console.log(time);
    var distS = [];  //creating distances array
    for (let i = 0; i < m.length; i++) {  //getting distances
        var dist = Math.round(parseInt(time) / 60 * parseInt(m[i].speed)*100)/100;
        distS.push(dist);  //pushing new calculated distance to array
        console.log(dist);
        console.log(distS);
    }

    var max = Math.max(...distS); //finding max distance
    for (let j = 0; j < distS.length; j++) {
        var ncarDiv = document.createElement('div');
        ncarDiv.innerHTML = m[j].name;
        console.log(ncarDiv);
        console.log(m[j]);
        var distDiv = document.createElement('div');
        distDiv.innerHTML = distS[j];
        var autoRow = document.createElement('div');
        if (distS[j] == max) {
            autoRow.classList.add('fastest');  // adding new class for styling
        }
        autoRow.classList.add('results');
        autoRow.appendChild(ncarDiv);
        autoRow.appendChild(distDiv);
        results.appendChild(autoRow);
    }
});
