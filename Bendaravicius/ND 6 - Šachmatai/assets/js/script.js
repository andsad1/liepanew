// Klasiu pridejimas visiems lentos langeliams
var cells = document.getElementsByClassName('cell');
for (var i = 0; i < cells.length; i++) {
    cells[i].setAttribute('ondrop','drop(event)');
    cells[i].setAttribute('ondragover','allowDrop(event)');
}

// Klasiu pridejimas visoms figuroms
var figures = document.getElementsByClassName('figure');
for (var i = 0; i < figures.length; i++) {
    figures[i].setAttribute('ondragstart','drag(event)');
    // Nustatomas draggable atributas taip, kad žaidimą pradėtų baltos figūros
    if (figures[i].classList.contains('white')) {
        figures[i].setAttribute('draggable','true');
    }
    else {
        figures[i].setAttribute('draggable','false');
    }
}

var id, parentId;

function drag(e) {
    id = e.target.id;
    parentId = e.target.parentElement.id;
}

function allowDrop(e) {
    var figura = document.getElementById(id);
    var kitaFigura = document.getElementById(e.target.id);
    /* IF'u neleidžiami ėjimai:
        • Ėjimas į tą patį langelį
        • Ėjimas į langelį užimtą kitos friendly figūros 
    */
    if (e.target.id == id || e.target.id == parentId || figura.classList.contains('white') && kitaFigura.classList.contains('white') ||  figura.classList.contains('black') && kitaFigura.classList.contains('black')) return;
    e.preventDefault();
}

function drop(e) {
    var element = document.getElementById(id);
    var target = document.getElementById(e.target.id);
    var moves = document.querySelector('.log');
    var j = 0;
    // Jeigu langelyje yra figūra ir ji nėra friendly figūra, tuomet vyksta kirtimas
    if (target.classList.contains('figure') && element.classList[1] != target.classList[1]) {
        var parent = document.getElementById(e.target.parentElement.id);
        // Kirtimas užrašomas į pirmą ėjimų istorijos eilutę
        var move = getComputedStyle(element, '::after').content.replace(/["]+/g, '') + ' ' + parentId + ' \u2694 ' + e.target.parentElement.id + ' ' + getComputedStyle(target, '::after').content.replace(/["]+/g, '') + '<br>';
        moves.innerHTML = move + moves.innerHTML;
        parent.removeChild(target);
        parent.appendChild(element);
    }
    else {
        var parent = document.getElementById(e.target.id);
        parent.appendChild(element);
        // Ėjimas užrašomas į pirmą ėjimų istorijos eilutę
        var move = getComputedStyle(element, '::after').content.replace(/["]+/g, '') + ' ' + parentId + ' > ' + e.target.id + '<br>';
        moves.innerHTML = move + moves.innerHTML;
    }

    if ((j + 2) % 2 == 0) {
        for (var i = 0; i < figures.length; i++) {
            // Nurodoma, kad sekantis ėjimas bus juodų figūrų
            if (figures[i].classList.contains('white')) {
                figures[i].setAttribute('draggable','false');
            }
            else {
                figures[i].setAttribute('draggable','true');
            }
        }
    }
    else {
        for (var i = 0; i < figures.length; i++) {
            // Nurodoma, kad sekantis ėjimas bus baltų figūrų
            if (figures[i].classList.contains('black')) {
                figures[i].setAttribute('draggable','true');
            }
            else {
                figures[i].setAttribute('draggable','false');
            }
        }
    }
}
