$(document).ready(function () {

    var table = $('#container');
    // Modalai
    var inputModal = $('#input-modal.modal');
    var confirmModal = $('#confirmation-modal.modal');

    // Buttonai
    var newItem = $('#add-btn');
    var delAll = $('#remove-btn');
    var saveBtn = $('#save-btn');
    var cancelBtn = $('#cancel-btn');
    // Patvirtinimo formos
    var delYes = $('#delete-yes-btn');
    var delNo = $('#delete-no-btn');

    // Inputai
    var marke = $('#marke-input');
    var number = $('#number-input');
    var speed = $('#speed-input');


    // Masyvas automobilių duomenų saugojimui
    var automobiliai = [];

    var selectedItem = '';

    // Klasė pagal kurią įvesti duomenys grupuojami į objektus
    class Auto {
        constructor(marke, numeris, greitis) {
            this.marke = marke;
            this.numeris = numeris;
            this.greitis = greitis;
        }
    }

    // Iškvietus input funkciją nustatyti input reikšmes pagal parametrus, kitaip - default ''

    // Iškviečiamas duomenų įvedimo modalas
    newItem.on('click', () => {
        inputModal.show();
    })



    // Naujo duomenų elemento sukūrimas
    saveBtn.on('click', () => {
        if (selectedItem != '') {
            var markeValue = $(selectedItem).children()[0].innerHTML;
            var numberValue = $(selectedItem).children()[1].innerHTML;
            var speedValue = $(selectedItem).children()[2].innerHTML;

            markeValue = marke.val();
            numerValue = number.val();
            speedValue = `${speed.val()} km/h`;

            selectedItem = '';
            // Duomenų inputuose išvalymas
            marke.val('');
            number.val('');
            speed.val('');

            inputModal.hide();
        }
        else if (marke.val() != '' && number.val() != '' && speed.val() != '') {
            var newMarke = $(`<div>${marke.val()}</div>`);
            var newNumber = $(`<div>${number.val()}</div>`);
            var newSpeed = $(`<div>${speed.val()} km/h</div>`);

            var btnWrap = $('<div class="btn-wrap"><button class="edit">keisti</button><button class="delete cancel">trinti</button></div>');
            var newRow = $('<div class="item"></div>');
            newRow.append(newMarke);
            newRow.append(newNumber);
            newRow.append(newSpeed);
            newRow.append(btnWrap);

            table.append(newRow);

            var newAuto = new Auto(marke.val(), number.val(), speed.val());
            automobiliai.push(newAuto);

            // Duomenų inputuose išvalymas
            marke.val('');
            number.val('');
            speed.val('');

            inputModal.hide();
        }
        else {
            alert('Užpildykite visus duomenis!');
        }
        // Iškviečimas duomenų įvedimo modalas duomenų koregavimui
        $('[class~=edit]').on('click', (e) => {
            selectedItem = e.target.parentElement.parentElement;
            var markeValue = $(selectedItem).children()[0].innerHTML;
            var numberValue = $(selectedItem).children()[1].innerHTML;;
            var speedValue = $(selectedItem).children()[2].innerHTML;
            var speedIntValue = parseInt(speedValue.substring(0, speedValue.length - 5));
            inputModal.show();

            marke.val(markeValue);
            number.val(numberValue);
            speed.val(speedIntValue);
        });
    });
    // Paspaudus 'trinti' pasirenkamas trinamas elementas ir iškviečiamas patvirtinimo modalas
    $('#container').on('click', '[class~=delete]', (e) => {
        selectedItem = e.target.parentElement.parentElement;
        confirmModal.show();
    });
    // Įrašymas į masyvą

    // Modalo uždarymas
    cancelBtn.on('click', () => {
        inputModal.hide();
        // Duomenų inputuose išvalymas
        marke.val('');
        number.val('');
        speed.val('');

        selectedItem = '';
    });

    // ===========================
    // Pasirinkto įrašo ištrinimas

    // Paspaudus 'taip' elementas ištrinamas
    delYes.on('click', () => {
        selectedItem.remove();
        confirmModal.hide();

        selectedItem = '';
    });

    // Paspaudus 'ne' patvirtinimo modalas uždaromas ir elementas lieka
    delNo.on('click', () => {
        confirmModal.hide();

        selectedItem = '';
    });

    // Visų duomenų išvalymas
    delAll.on('click', () => {
        table.children().remove();
    })
});

