var kvadratas = document.getElementById('kvadratas');

function toggle() {
    if (kvadratas.classList.contains('raudonas')) {
        kvadratas.classList.remove('raudonas');
        kvadratas.classList.add('zalias');
    }
    else {
        kvadratas.classList.remove('zalias');
        kvadratas.classList.add('raudonas');
    }
};

kvadratas.addEventListener('click', toggle);

