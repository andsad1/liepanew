var addBtn = document.getElementById('add');
addBtn.addEventListener('click', addItem);

var taskList = document.getElementById('list');
var task = document.getElementById('new-item');
task.addEventListener('keydown', addItemEnter);

// Pasirenkamas <h2> elementas esantis virš darbų sąrašo
var heading = document.getElementById('list-heading');
// Funkcija tikrinanti ar yra darbų ir keičianti <h2> užrašą atitinkamai
function check() {
    if (taskList.childNodes.length == 0) {
        heading.innerHTML = 'There is nothing to do!';
    }
    else {
        heading.innerHTML = 'Time to get working!';
    }
}

function addItem() {
    if (task.value != '') {
        // Sukuriam <div> užduočiai, pridedame X mygtuką ir užpildome tekstu iš <input>
        var newItem = document.createElement('div');
        var closeBtn = '<i class="fas fa-times"></i>';
        newItem.innerHTML = task.value + closeBtn;
        newItem.classList.add('task');

        // Funkcija leidžianti pažymėti atliktas užduotis
        newItem.addEventListener('click', function() {
            if (newItem.classList.contains('done') != true) {
                newItem.classList.add('done');
            }
            else {
                newItem.classList.remove('done');
            }
        });

        // Funkcija ištrinanti elementą paspaudus X
        newItem.childNodes[1].addEventListener('click', function(e) {
            newItem.remove();
            check();    
        });

        // Įtraukiam užduotį į sąrašą
        taskList.appendChild(newItem);

        // Ištrinam <input> lauko turinį
        task.value = '';
    } else {return;}
    check();
}

// Funkcija objekto pridėjimui nuspaudus 'Enter'
function addItemEnter(e) {
    if (e.keyCode === 13) {
        addItem();
    }
}