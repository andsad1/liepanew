/* Figuru duomenys
   Kiekvienos figuros duomenys surasyti objekte, kuris patalpyintas figuru masyve*/
var figuros = [
    // pirma figura
    {
        ilgis: 2,
        plotis: 5,
        aukstis: 3
    },
    // antra figura
    {
        ilgis: 3,
        plotis: 2,
        aukstis: 6
    },
    // trecia figura
    {
        ilgis: 1,
        plotis: 5,
        aukstis: 5
    }
];

// Mygtuku wrapper'io pasirinkimas
var buttons = document.querySelector('.btn-wrap');

// Mygtuku sukurimas
var btn1 = document.createElement('button');
btn1.innerHTML = 'Išvesti duomenis';

var btn2 = document.createElement('button');
btn2.innerHTML = 'Rasti didžiausią tūrį';
// Mygtuko nr.2 css klases pridejimas
btn2.classList.add('calculate');

// Mygtuku talpinimas wrapper'yje
buttons.appendChild(btn1);
buttons.appendChild(btn2);

// Duomenu isvedimo mygtuko on-click funkcija
btn1.addEventListener('click', function(){
    for (var i = 0; i < figuros.length; i++) {
        let figura = figuros[i];
        // Lenteles eilutes ir joje esanciu langeliu kurimas
        let newRow = document.createElement('tr');
        for (var j = 0; j < 4; j++) {
            newRow.appendChild(document.createElement('td'));
        };
        // Figuros [i] duomenu suvedimas i eilutes langelius
        newRow.childNodes[0].innerHTML = figura.ilgis;
        newRow.childNodes[1].innerHTML = figura.plotis;
        newRow.childNodes[2].innerHTML = figura.aukstis;
        // Eilutes su suvestais duomenimis itraukimas i lentele
        lentele.appendChild(newRow);
    };
    // Mygtuko veikimo isjungimas
    this.setAttribute('disabled', 'true');

    console.log('Duomenis suvesti i lentele!');
});

// Turio skaiciavimo ir didziausios vertes radimo mygtuko on-click funkcija
btn2.addEventListener('click', function(){
    // Patikrinimas ar i lentele yra isvesti duomenys
    if (lentele.childNodes.length > 1) {
        // Turiu skaiciavimas ir surasymas i lentele
        for (var i = 0; i < figuros.length; i++) {
            let figura = figuros[i];
            let turis = figura.ilgis * figura.plotis * figura.aukstis;
            lentele.childNodes[i + 1].childNodes[3].innerHTML = turis;
        }
        let turis1 = lentele.childNodes[1].childNodes[3].innerHTML;
        let turis2 = lentele.childNodes[2].childNodes[3].innerHTML;
        let turis3 = lentele.childNodes[3].childNodes[3].innerHTML;
        console.log('Tūriai suskaičiuoti ir suvesti į lentelę!')

        // Didziausio turio radimas
        if (turis1 > turis2 && turis1 > turis3) {
            lentele.childNodes[1].classList.add('red');
            console.log('Didžiausias tūris yra: ' + turis1);
        }
        else if (turis2 > turis1 && turis2 > turis3) {
            lentele.childNodes[2].classList.add('red');
            console.log('Didžiausias tūris yra: ' + turis2);
        }
        else {
            lentele.childNodes[3].classList.add('red');
            console.log('Didžiausias tūris yra: ' + turis3);
        }
    // Perspejimas, jeigu duomenys nera isvesti
    } else {
        alert('NĖRA DUOMENŲ SKAIČIAVIMUI!\n\nPirmiausia paspauskite duomenų išvedimo mygtuką!')
    }
});

// Lenteles <tbody> elemento pasirinkimas
var lentele = document.querySelector('tbody');

// maxTuris.classList.add('red');