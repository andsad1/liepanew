// 6. Write a JavaScript program which compute, the average marks of the following students Then, this average is used to determine the corresponding grade.

var studentGrades = [['David',80],['Vinoth',77],['Divya',88],['Ishitha',95],['Thomas',68],];

for (var i = 0; i < studentGrades.length; i++) {
    if (studentGrades[i][1] < 60) {
        studentGrades[i].splice(1,1,'F')
    }
    else if (studentGrades[i][1] < 70) {
        studentGrades[i].splice(1,1,'D')
    }
    else if (studentGrades[i][1] < 80) {
        studentGrades[i].splice(1,1,'C')
    }
    else if (studentGrades[i][1] < 90) {
        studentGrades[i].splice(1,1,'B')
    }
    else {
        studentGrades[i].splice(1,1,'A')
    }
    
    console.log(studentGrades[i][0] + "'s grade is: " + studentGrades[i][1]);
}
