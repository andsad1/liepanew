// 1. Write a JavaScript program that accept two integers and display the larger. 

function getInput() {
    var num1 = prompt('Įveskite pirmąjį sveikąjį skaičių:');
    var num2 = prompt('Įveskite antrąjį sveikąjį skaičių:');

    return [num1, num2];
}

var skaiciai = getInput();
var skaicius1 = Number(skaiciai[0]);
var skaicius2 = Number(skaiciai[1]);

if (Number.isInteger(skaicius1) == true && Number.isInteger(skaicius2) == true) {
    if (skaicius1 > skaicius2) {
        console.log(skaicius1);
    }
    else if (skaicius1 < skaicius2) {
        console.log(skaicius2);
    }
    else if (skaicius1 == skaicius2) {
        console.log('Abu skaičiai lygūs.');
    }
}
else {
    alert('Iveskite du sveikuosius skaicius.')
    getInput();
}
