// 3. Write a JavaScript conditional statement to sort three numbers. Display an alert box to show the result.

var skaiciai = [0, -1, 4];
console.log('Pradiniai skaiciai: ' + skaiciai.toString());
var sorted = [];

// Pirmo skaiciaus irasymas
sorted.push(skaiciai[0]);

// Antro skaiciaus irasymas
if (skaiciai[1] < skaiciai[0]) {
    sorted.push(skaiciai[1]);
}
else {
    sorted.unshift(skaiciai[1]);
}

// Trecio skaiciaus irasymas
if (skaiciai[2] < skaiciai[0] && skaiciai[2] < skaiciai[1]) {
    sorted.push(skaiciai[2]);
}
else if (skaiciai[2] > skaiciai[0] && skaiciai[2] > skaiciai[1]){
    sorted.unshift(skaiciai[2]);
}
else {
    sorted.splice(1,0,skaiciai[2]);
}

// Rezultato spausdinimas
console.log('Isrykiuoti skaiciai: ' + sorted.toString());