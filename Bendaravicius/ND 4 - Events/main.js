// input var'ai
var data = document.getElementById('data');
var numeriai = document.getElementById('numeriai');
var atstumas = document.getElementById('dist');
var laikas = document.getElementById('laikas');
var submit = document.getElementById('submit');

var lentele = document.querySelector('#lentele tbody');

// Sukuria nauja tuscia eilute su 5 langeliais
function createRow() {
    var newRow = lentele.insertRow(-1);
    for (var i = 0; i < 5; i++) {
        var newCell = newRow.insertCell(0);
    }
}
// Istrina paskutine duomenu eilute lenteleje
function deleteData() {
    var pos = lentele.childNodes.length - 1;
    lentele.removeChild(lentele.childNodes[pos]);
}

// Isvalo inputus
function clearInput() {
    data.value = '';
    numeriai.value = '';
    atstumas.value = '';
    laikas.value = '';
}

var td = document.getElementsByTagName('td');

function writeData(e) {
    if (data.value != '' && numeriai.value != '' && atstumas.value != '' && laikas.value != '') {
        createRow();

        var lastItem = lentele.childNodes.length - 1;
        var currentRow = lentele.childNodes[lastItem];

        // Duomenu irasymas is Input elementu i lentele
        currentRow.childNodes[0].innerText = data.value;
        currentRow.childNodes[1].innerText = numeriai.value;
        currentRow.childNodes[2].innerHTML = atstumas.value;
        currentRow.childNodes[3].innerHTML = laikas.value;
        currentRow.childNodes[4].innerHTML = (atstumas.value / laikas.value).toFixed(2);

        clearInput();
    } else {
        alert('Uzpildykite visus duomenis');
    }
}