$(document).ready(function () {
    var container = $('#container');
    var colors = $('#color');
    var widthInput = $('#width');
    var heightInput = $('#height');

    $('#add').on('click', function () {

        let width = widthInput.val();
        let height = heightInput.val();
        let color = colors.val();

        if (color == 'random') {
            let randomColor = Math.floor(Math.random() * 4);
            switch (randomColor) {
                case 0:
                    color = 'red';
                    break;
                case 1:
                    color = 'blue';
                    break;
                case 2:
                    color = 'green';
                    break;
                case 3:
                    color = 'pink';
                    break;
            }
        }
        let element = $(`<div style="width: ${width}px; height: ${height}px; background-color: ${color}" class="block"><p>${width} x ${height}</p></div>`);
        container.append(element);
    });
    $('#container').on('click', '.block', function () {
        $(this).animate({
            left: '15px',
            bottom: '10px',
            opacity: '60%'
        }, 100, function () {
            $(this).animate({
                left: '0',
                right: '15px',
                bottom: '20px',
                opacity: '30%'
            }, 100, function () {
                $(this).animate({
                    left: '15px',
                    right: '0px',
                    bottom: '30px',
                    opacity: '0%'
                }, 100);
            });
        });
    });
});