var pwBox = document.getElementById('pw-box');
var newPw = document.getElementById('new-pw');
var repeat = document.getElementById('repeat-pw');

var submitBtn = document.getElementById('submit-pw');
var form = document.getElementById('form');
form.addEventListener('submit', function(e){
    e.preventDefault();
    if (newPw.value == '') {
        alert('Enter your new password.')
    }
    else if (repeat.value == '') {
        alert('Repeat your new password.')
    }
    else if (newPw.value != repeat.value) {
        alert('The passwords do not match!');
        newPw.classList.add('error');
        repeat.classList.add('error');
    }
    else {
        pwBox.classList.add('show');
        setTimeout(hideLoad, 2800);
    }
});

function hideLoad() {
    pwBox.classList.remove('show');
}

function removeError(e) {
    if (e.target.classList.contains('error')){
        e.target.classList.remove('error');
    }
}

newPw.addEventListener('focus', removeError);
repeat.addEventListener('focus', removeError);