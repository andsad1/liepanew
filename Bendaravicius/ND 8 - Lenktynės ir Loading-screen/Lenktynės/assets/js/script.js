var carName = document.getElementById('car-name');
var carSpeed = document.getElementById('car-speed');
var carList = document.getElementById('car-list');
var resultList = document.getElementById('result-list');

class Car {
    constructor(name, speed) {
        this.name = name;
        this.speed = speed;
        this.finishTime = 0;
    }
}

var cars = [];

var addBtn = document.getElementById('add-car');
addBtn.addEventListener('click', function() {
    if (carName.value != '' && carSpeed.value != '') {
        var item = document.createElement('div');
        item.classList.add('item');

        var newCar = new Car(carName.value, carSpeed.value);
        cars.push(newCar);

        var name = document.createElement('div');
        name.innerHTML = newCar.name;
        var speed = document.createElement('div');
        speed.innerHTML = newCar.speed + ' km/h';

        item.appendChild(name);
        item.appendChild(speed);

        carList.appendChild(item);

        var count = document.getElementById('car-count');
        count.innerHTML = carList.childNodes.length;

        carName.value = '';
        carSpeed.value = '';
    }
    else if (carName.value == '' && carSpeed.value == '') {
        alert("Enter the car's NAME and SPEED in km/h.");
    }
    else if (carName.value == '') {
        alert("Enter the car's NAME.");
    }
    else {
        alert("Enter the car's SPEED in km/h.");
    }
})

var distance = document.getElementById('race-distance');
var winner = document.getElementById('win-name');
var raceBtn = document.getElementById('start');
raceBtn.addEventListener('click', function(){
    if (cars.length != 0 && distance.value != '') {
        var results = document.getElementById('results');
        results.style.display = 'block';

        var bestTime = 99999999;
        
        for (var i = 0; i < cars.length; i++) {
            var item = document.createElement('div');
            item.classList.add('item');

            var name = document.createElement('div');
            name.innerHTML = cars[i].name;

            var time = document.createElement('div');
            var speed = cars[i].speed;

            var finishTime = distance.value / (speed / 3600);
            cars[i].finishTime = finishTime;
            console.log(cars[i]);
            
            var hours = Math.floor(finishTime / 3600);
            var minutes = Math.floor((finishTime - hours*3600) / 60);
            var seconds = finishTime - (hours * 3600) - (minutes * 60);
            time.innerHTML = hours + ' h ' + minutes + ' m ' + seconds.toFixed(2) + ' s';

            if (finishTime <= bestTime) {
                bestTime = finishTime;
            }

            item.appendChild(name);
            item.appendChild(time);

            resultList.appendChild(item);
        }

        for (var i = 0; i < cars.length; i++) {
            if (cars[i].finishTime == bestTime){
                resultList.childNodes[i].classList.add('winner');
                winner.innerHTML = cars[i].name;
            }
        }

        distance.value = '';
    }
    else if (carList.childNodes.length == 0) {
        alert('You need to add some participants first!');
    }
    else {
        alert('Enter the race distance in km.');
    }
});
