$(document).ready(function(){

    var automobiliai = [];

    function lentele (){
        $("table tbody").empty();
        for(let i = 0; i <automobiliai.length; i++){
            var tr = $("<tr></tr>")
            var autNrTd = $("<td>" + automobiliai[i].autNumeris + "</td>");
            var autModelisTd = $("<td>" + automobiliai[i].modelis + "</td>");
            var greitisTd = $("<td>" + automobiliai[i].greitis + "</td>");
            var taisytiTd = $("<td><button class='taisytiMygtukas btn btn-success'>Taisyti</button></td>");
            var istrintiTd = $("<td><button class='istrintiMygtukas btn btn-danger'>Naikinti</button></td>");
            tr.append(autNrTd).append(autModelisTd).append(greitisTd).append(taisytiTd).append(istrintiTd);
            $("table tbody").append(tr);
        }
    }

    lentele();

    $("#naujasBtn").on("click", function(){
        $(".modalas").fadeIn(100)
        $(".btnSaugoti").css({
            "display": "block"
        })
        $(".btnTaisyti").css({
            "display": "none"
        })
    })
    $("table").on("click", ".istrintiMygtukas", function(){
        var index = $(this).parent().parent().index();
        automobiliai.splice(index, 1);
        lentele();
    });

    var rowIndex
    $("table").on("click", ".taisytiMygtukas", function () {
        $(".modalas").fadeIn(100);
        $(".btnSaugoti").css({
            "display": "none"
        })
        $(".btnTaisyti").css({
            "display": "block"
        })
        
        rowIndex = $(this).parent().parent().index();
        
        $(".autNr").val(automobiliai[rowIndex].autNumeris);
        $(".modelis").val(automobiliai[rowIndex].modelis);
        $(".greitis").val(automobiliai[rowIndex].greitis);  

    })
    $(".modalas").on('click', '.atsaukti', function () {
        $('.modalas').fadeOut(100);
    });
    $(".modalas").on("click", ".btnSaugoti", function () {
        var autNr = $(".autNr").val();
        var autoModelis = $(".modelis").val();
        var autoGreitis = $(".greitis").val();
       
        var naujiDuomenys = {
            autNumeris: autNr,
            modelis: autoModelis,
            greitis: autoGreitis
        }
        automobiliai.push(naujiDuomenys);
        console.log(automobiliai)
        
        lentele();
        $(".autNr").val("");
        $(".modelis").val("");
        $(".greitis").val("");
    })
    
    $(".modalas").on("click", ".btnTaisyti", function () {
        var autNr = $(".autNr").val();
        var autoModelis = $(".modelis").val();
        var autoGreitis = $(".greitis").val();
        
        var naujiDuomenys = {
            autNumeris: autNr,
            modelis: autoModelis,
            greitis: autoGreitis
        }
        automobiliai.splice(rowIndex, 1 ,naujiDuomenys);
        console.log(automobiliai)
        
        lentele();
        
    })
    
    

});