// Datos
var d = new Date(1849312452).toLocaleDateString("en-US");
console.log(d);

var auto = {
    numeris: 'ABC123',
    laikas: 100,
    kelias: 200,
    greitis: function() {
        return this.kelias / this.laikas * 3.6;
    }
}
console.log(auto.greitis());


function Automobilis(nr, s, t) {
    this.numeris = nr;
    this.laikas = s;
    this.kelias = t;
    this.kitasGreitis = this.kelias / this.laikas * 3.6;
    this.greitis = function() {
        return this.kelias / this.laikas * 3.6;
    }
}
class Zmogus {
    constructor(x, y, z) {
        this.vardas = x;
        this.amzius = y;
        this.lytis = z;
    }
}
var antanas = new Zmogus('Antanas', 58, 'vyras');

Zmogus.prototype.gimimoMetai = function() {
    return 2020 - this.amzius;
}
console.log(antanas.gimimoMetai());
var bmw = new Automobilis('ABC123', 200, 4021);
var volvo = new Automobilis('FSS233', 300, 1021);
var mb = new Automobilis('FKS643', 400, 3021);
var wv = new Automobilis('WOR234', 200, 2021);
Automobilis.prototype.greitisms = function() { return this.kelias / this.laikas };
console.log(bmw.greitis());
console.log(volvo.greitis());
console.log(mb.greitis());
console.log(wv.greitisms());
