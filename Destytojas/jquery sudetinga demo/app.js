$( document ).ready(function() {

    var duomenys = [
        {
            pav: 'vienas',
            spalva: 'raudona',
            skaiciai: 66456 
        },
        {
            pav: 'du',
            spalva: 'zalia',
            skaiciai: 5687 
        },
        {
            pav: 'trys',
            spalva: 'geltona',
            skaiciai: 75745 
        },
        {
            pav: 'keturi',
            spalva: 'melyna',
            skaiciai: 43234 
        }
    ]
    // sukuriama funkcija, kuri mums ja iskvietus nupies lentele is duomenu masyvo
    function lentele () {
        $('table tbody').empty();
        for (i = 0; i < duomenys.length; i ++) {
            // console.log(duomenys[i].skaiciai);
            var tr = $('<tr></tr>')
            var pavTd = $('<td>'+ duomenys[i].pav +'</td>');
            var spalvTd = $('<td>'+ duomenys[i].spalva +'</td>');
            var skaicTd = $('<td>'+ duomenys[i].skaiciai +'</td>');
            var editBtn = $('<td><button class="edit_button btn btn-info"">Keisti</button></td>');
            var deleteBtn = $('<td><button class="delete_button btn btn-danger">Istrinti</button></td>');
            tr.append(pavTd).append(spalvTd).append(skaicTd).append(editBtn).append(deleteBtn);
            $('table tbody').append(tr);
    
        }
    }
    // uzkrovus puslapi nupies lenetele
    lentele();
    
    // trinti duomenis paspaudus ant istrinti mygtuko
    $('table').on('click', '.delete_button', function(){
        // pasiemam tr'o indexa, kuriame yra mygtukas ant kurio paspaudem
        var index = $(this).parent().parent().index();
        // isnaikina viena objekta (siuo atveju) is duomenu masyvo pagal pries tai pasiimta index kintamaji
        duomenys.splice(index, 1);
        // iskvieciam funkcija, kad is naujo perpiestu lentele
        lentele();
    });
    // kintamasis, kuri pasiimsim kviesdami edit modala, (jis global scope, kad galetume pasiekti visose fukcijose ir t.t.)
    var rowIndex;
    $('table').on('click', '.edit_button', function () {
        $('#musu_modal').fadeIn(200);
        // pasiemam tr'o indexa, kuriame yra mygtukas ant kurio paspaudem
        rowIndex = $(this).parent().parent().index();
        // nustatom modalo inputam tokius pacius duomenis, kurie yra lenteleje
        $('#musu_modal .pav_input').val(duomenys[rowIndex].pav);
        $('#musu_modal .spalv_input').val(duomenys[rowIndex].spalva);
        $('#musu_modal .skaic_input').val(duomenys[rowIndex].skaiciai);
    })
    // paspaudus atsaukti paslempiam modala
    $('#musu_modal').on('click', '.cancel', function () {
        $('#musu_modal').fadeOut(200);
    })
    // modale spaudziam mygtuka saugoti
    $('#musu_modal').on('click', '.save', function () {
        // pasiemam duomenis is inputu
        var pavNew = $('.pav_input').val();
        var spalvNew = $('.spalv_input').val();
        var skaicNew = $('.skaic_input').val();
        // suskuriam nauja objekta su pasirinktais duomenim
        var naujiDuomenys = {
            pav: pavNew,
            spalva: spalvNew,
            skaiciai: skaicNew
        }
        // is masyvo isimam redaguojama objekta ir i jo vieta idedam naujai sukurta objekta (masyvo index, kiek elementu salinam, kuo pakeiciam pasalinta elementa)
        duomenys.splice(rowIndex, 1, naujiDuomenys);
        // kvieciam funkcija, kad perspiesti lentele
        lentele();
        $('#musu_modal').fadeOut(200);
    })
});