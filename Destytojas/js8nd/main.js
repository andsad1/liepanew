// Pražioje sukurti 2 input fieldus(automobilio pavadinimas ir greitis), įvedus informaciją ir paspaudus pridėti - sudėti duomenis į lentelę arba paprastą div'ą(kiekvieną kart paspaudus prisideda vis nauji duomenys, seni neišsitrina).
// Žemiau sukurti input fieldą laikas, ir paspaudus mygtuką lenktyniauti, palyginti automobilių nuvažiuotą atstumą lentelėje).
// Automobilių sukūrimui panaudoti funkciją - konstruktorių.
// ** sudedant duomenis į lentelę, paryškinti didžiausią atstumą nuvažiavusį automobilį.

// Nuotraukuoje pavizdys kaip galėtų atrodyti, kad lengviau įsivaizduoti.
function Auto(p, g) {
    this.pavadinimas = p;
    this.greitis = g;
}
var automobiliai = [];
// automobiliai[0].Auto.prototype.atstumas = atstumas;
// Psiemam visą formą
var duomenys = document.getElementById('duomenys');
var lenktyniauti = document.getElementById('lenktyniauti');
var autoInfo = document.getElementById('auto-info');
duomenys.addEventListener('submit', function (e) {
    e.preventDefault();
    // Pasiemam duomenis is inputu
    var pavadinimas = document.getElementById('pavadinimas').value;
    var greitis = document.getElementById('greitis').value;
    // Sukuriam automobilio objekta, pagal Auto konstruktoriu
    var auto = new Auto(pavadinimas, greitis);
    // sudedam automobilius i masyva
    automobiliai.push(auto);
    // sukuriam pavadinimo div'a
    var pavDiv = document.createElement('div');
    // idedam teksta i pavadinimo div'a
    pavDiv.innerHTML = pavadinimas;
    // sukuriam greicio div'a
    var greitDiv = document.createElement('div');
    // idedam teksta i greicio div'a
    greitDiv.innerHTML = greitis;
    // susikuriam auto row div'a
    var autoRow = document.createElement('div');
    // uzdedam klase auto row div'ui, kad galetume ji pastiliuot
    autoRow.classList.add('auto-row');
    // sudedam pavadinima ir greiti i auto row diva
    autoRow.appendChild(pavDiv);
    autoRow.appendChild(greitDiv);
    // sudedam pilna auto row div'a i autoinfo div'a
    autoInfo.appendChild(autoRow);
});
var rezultatai = document.getElementById('rezultatai');
lenktyniauti.addEventListener('submit', function (e) {
    e.preventDefault();
    // Pasiemam ivesta laika
    var laikas = document.getElementById('laikas').value;
    var atstumai = [];
    // Suskaiciuojam atstumus
    for (let i = 0; i < automobiliai.length; i++) {
        var atstumas = parseInt(laikas) * parseInt(automobiliai[i].greitis);
        atstumai.push(atstumas);
    }
    // Randam didziausia reiksme
    var max = Math.max(...atstumai);
    for (let j = 0; j < atstumai.length; j++) {
        var pavDiv = document.createElement('div');
        pavDiv.innerHTML = automobiliai[j].pavadinimas;
        var atstumDiv = document.createElement('div');
        atstumDiv.innerHTML = atstumai[j];
        var autoRow = document.createElement('div');
        // Patikrinam kurie atstumai sutampta su didziausia reiksme
        if (atstumai[j] == max) {
            // Sita klase kuriam del stiliavimo
            autoRow.classList.add('greiciausias');
        }
        autoRow.classList.add('auto-row');
        // sudedam pavadinima ir greiti i auto row diva
        autoRow.appendChild(pavDiv);
        autoRow.appendChild(atstumDiv);
        rezultatai.appendChild(autoRow);
    }
});