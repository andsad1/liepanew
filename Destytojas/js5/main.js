var masyvas = [1, 2, 3, 'labas'];
masyvas[2];
var zmogus = {
    vardas: 'Petras',
    gimimoData: 1954,
    lytis: 'vyras',
    vedes: true,
    draugai: [
        {
            vardas: 'Antanas',
            lytis: 'vyras',
            pomegiai: ['Futbolas', 'Zvejyba', 'Masinos', 'Maistas']
        },
        {
            vardas: 'Angelė',
            lytis: 'moteris',
            pomegiai: ['Krepsinis', 'Zurnalai']
        },
        {
            vardas: 'Jonas',
            lytis: 'vyras',
            pomegiai: ['Sokt', 'Linuxai']
        }
    ]
}
// Sukam cikla per draugu masyva
for (var i = 0; i < zmogus.draugai.length; i++) {
    // Sukam cikla per draugu pomegiu masyva
    for (var j = 0; j < zmogus.draugai[i].pomegiai.length; j++) {
        console.log(zmogus.draugai[i].pomegiai[j]);
    }
}