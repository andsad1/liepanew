var figuros = [
    {
        ilgis: 10,
        plotis: 20,
        aukstis: 15
    },
    {
        ilgis: 11,
        plotis: 30,
        aukstis: 17
    },
    {
        ilgis: 16,
        plotis: 23,
        aukstis: 10
    },
    {
        ilgis: 30,
        plotis: 24,
        aukstis: 10
    },
    {
        ilgis: 16,
        plotis: 17,
        aukstis: 10
    },
    {
        ilgis: 10,
        plotis: 20,
        aukstis: 15
    },
    {
        ilgis: 11,
        plotis: 30,
        aukstis: 17
    },
    {
        ilgis: 16,
        plotis: 23,
        aukstis: 10
    },
    {
        ilgis: 30,
        plotis: 24,
        aukstis: 10
    },
    {
        ilgis: 16,
        plotis: 17,
        aukstis: 10
    }
]
console.log(figuros);
var duomenys = document.getElementById('duomenys');
var rastiMax = document.getElementById('rasti_max');
// var masyvas = [1, 7, 10, 30, 10];
// ... siuo atveju nuima nuo masyvo lauztinius skliaustus
// console.log(Math.max(...masyvas));

// Sudedam duomenis i lentele
var tbody = document.querySelector('tbody');
var turiai = [];
duomenys.addEventListener('click', function () {
    for (var i = 0; i < figuros.length; i++) {
        var turis = figuros[i].aukstis * figuros[i].ilgis * figuros[i].plotis;
        var tableRow = document.createElement('tr');
        var ilgisTd = document.createElement('td');
        var plotisTd = document.createElement('td');
        var aukstisTd = document.createElement('td');
        tableRow.appendChild(ilgisTd).innerHTML = figuros[i].ilgis;
        tableRow.appendChild(plotisTd).innerHTML = figuros[i].plotis;
        tableRow.appendChild(aukstisTd).innerHTML = figuros[i].aukstis;
        tbody.appendChild(tableRow);
        turiai.push(turis);
    }
    console.log(turiai);
});

rastiMax.addEventListener('click', function () {
    tbody.innerHTML = ''; 
    // Susirandam max reiksme
    var max = 0;
    for (var i = 0; i < turiai.length; i++) {
        if (turiai[i] > max) {
            max = turiai[i];
        }
    }
    // Spausdinam viska is naujo i lentele ir tuo paciu metu patikrinam, kurie turiai sutampta su max reiksme
    for (var k = 0; k < turiai.length; k++) {
        var tableRow = document.createElement('tr');
        var ilgisTd = document.createElement('td');
        var plotisTd = document.createElement('td');
        var aukstisTd = document.createElement('td');
        var turisTd = document.createElement('td');
        if (turiai[k] == max) {
            turisTd.classList.add('max');
        }
        tableRow.appendChild(ilgisTd).innerHTML = figuros[k].ilgis;
        tableRow.appendChild(plotisTd).innerHTML = figuros[k].plotis;
        tableRow.appendChild(aukstisTd).innerHTML = figuros[k].aukstis;
        tableRow.appendChild(turisTd).innerHTML = turiai[k];
        tbody.appendChild(tableRow);
    }
});
// console.log(turiai);
// console.log(Math.max(...turiai));
