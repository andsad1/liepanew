$(document).ready(function () {
    $('.delay').delay(2000).fadeOut(2000).delay(3000).fadeIn(2000);
    $('p').slideDown(1000, function(){console.log('vienas')}).html('tekstas 123').addClass('klase vienas dar_kita');
    console.log($('p').html());
    $('.antras').attr('id', 'paragrafas').css({
        'background': 'red',
        'color': '#fff',
        'font-size': '30px'
    });
    console.log($('p').width());
    var elementas = $('<div class="naujas_elementas">kazkoks tai musu tekstas</div>');
    $('#eventai').append(elementas);
    // $('.naujas_elementas').click(function(){
    //     console.log('veikia');
    //     $(this).toggleClass('spalva');
    // });
    // $('p').on('click mouseover mouseleave', function(){
    //     $(this).next().toggleClass('spalva').fadeOut(500).fadeIn(500);
    // });
    $('#eventai').on('click', 'p', function () {
        console.log('veikia');
        $(this).toggleClass('spalva');
        $(this).animate(
            {
                width: '200px',
                margin: '40px',
                tranform: 'translateX(-200px)'
            }, 3000, function () {
                $(this).delay(3000).animate(
                    {
                        width: '400px',
                        margin: '80px',
                        opacity: 0
                    }, 3000, 'swing', function () {
                        $(this).animate(
                            {
                                width: '100px',
                                margin: '10px',
                                opacity: 1
                            }, 3000)
                    }
                )
            }
        )
    });
});