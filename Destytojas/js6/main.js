// Global Scope
// var b = 'tekstas';
// let c = 100;
function musuFunkcija() {
    // Function Scope/Local scope
    // c = 'kitas tekstas';
}

musuFunkcija();
// console.log(c);

if (true) {
    // Block Scope
    let a = 100;
}
// console.log(a);

const e = [1, 2, 6, 4, 3];
e.push('tekstas');
console.log(e);


// function dalyba(x) {
//     return x / 2;
// }
var dalyba = function (x) {
    return x / 2;
}

var kubu = (x) => {
    return x * x * x;
}
var daugyba = (x) => x * 2;
console.log(daugyba(2));

if (4 > 2) console.log(daugyba(10));

function map(funkcija, masyvas) {
    var result = [];
    for (var i = 0; i < masyvas.length; i++) {
        result[i] = funkcija(masyvas[i]);
    }
    return result;
}

var kvadratu = (x) => x * x;

console.log(map(kvadratu, [1, 2, 3, 4]));



function faktorial(n) {
    if (n == 1) return 1;
    return n * faktorial(n - 1);
}

console.log(faktorial(120));