var forma = document.getElementById('prideti');
forma.addEventListener('submit', function(){
    event.preventDefault();
    // Sukuriam todo elementa
    var reiksme = document.getElementById('reiksme').value;
    var todos = document.getElementById('todos');
    var todo = document.createElement('div');
    todo.classList.add('todo');
    todo.innerHTML = reiksme;
    // Sukuriam delete mygtuka ir ji idedam i todo elementa
    var remove = document.createElement('button');
    remove.classList.add('delete');
    remove.innerHTML = 'Delete';
    todo.appendChild(remove);
    // idedam todo elementa i todos lista
    todos.appendChild(todo);
    document.getElementById('reiksme').value = '';
    // galim pazymeti kaip padaryta
    todo.addEventListener('click', function(){
        todo.classList.toggle('done');
    });
    // istrinti todo elementa
    remove.addEventListener('click', function(){
        remove.parentElement.remove();
    });
})
