let categorySelect = document.getElementById('category');
let difficultySelect = document.getElementById('difficulty');

let request = new XMLHttpRequest();
let questions;
let restartButton = document.querySelector('.restartButton');
let startGame = document.querySelector('.begin')

let gameData = {
    score: 0,
    currentQuestion: 0,
    category: '',
    difficulty: 'easy'
}

let restUrl = 'https://opentdb.com/api.php?amount=10&type=boolean';



categorySelect.addEventListener('change', () => {
    console.log('Getting questions');
    gameData.category = categorySelect.value;
//    getQuestions();
    
})

difficultySelect.addEventListener('change', () => {
    gameData.difficulty = difficultySelect.value;
})

startGame.addEventListener('click', () => {
    start();
    return $(document).ready(function () {
        $('.mainPage').hide();
    });
});

request.onreadystatechange = function () {
    switch (request.readyState) {
        case 1:
            //Setting loader
            document.querySelector('.loader').style.display = 'block';
            break;

        case 4:
            //Request DONE
            questions = JSON.parse(request.responseText);
            questions = questions.results;

            console.log(questions);
            document.querySelector('.loader').style.display = 'none';
            drawCurrentQuestion();
            break;
    }
};

request.open('GET', restUrl);
request.send();

function selectAnswer(option) {
    let correctAnswer = questions[gameData.currentQuestion].correct_answer;

    if (option === correctAnswer) {
        gameData.score++;
    }

    if (gameData.currentQuestion < 9) {
        gameData.currentQuestion++;
        drawCurrentQuestion();
        document.querySelector('.score').innerHTML = `${gameData.currentQuestion}/10`;

    } else {
        return $(document).ready(function () {
            $('.finish').show();
            document.querySelector('.finalScore').innerHTML = `${gameData.score * 10}%`;
        });
    };
    
    if (gameData.score >= 6) {
        return $(document).ready(function () {
            $('.good').show();
            $('.bad').hide();
        });
    }
        else {
        return $(document).ready(function () {
            $('.bad').show();
            $('.good').hide();
        });
    }
};

restartButton.addEventListener('click', () => {
    return $(document).ready(function () {
        $('.finish').hide();
        $('.mainPage').show();
    });
});


function drawCurrentQuestion() {
    let questionElement = document.querySelector('.question');
    questionElement.innerHTML = questions[gameData.currentQuestion].question;
}

function start() {
    gameData.score = 1;
    gameData.currentQuestion = 1;
    document.querySelector('.score').innerHTML = `${gameData.score}/10`;
    restUrl += `&category=${gameData.category}`;
    restUrl += `&difficulty=${gameData.difficulty}`;
    request.open('GET', restUrl);
    request.send();
    drawCurrentQuestion();
};

console.log(restUrl);
