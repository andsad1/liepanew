
var figuros = [
    { ilgis: 2, plotis: 5, aukstis: 3 },
    { ilgis: 3, plotis: 2, aukstis: 6 },
    { ilgis: 1, plotis: 5, aukstis: 5 }
];
console.log(figuros);

// Pasiimam mygtuka
var mygtukas1 = document.getElementById('mygtukas1');
var mygtukas2 = document.getElementById('mygtukas2');
// Pasiimam tbody
var tbody = document.querySelector('tbody');
// Sukuriu veiksmą mygtuko paspaudimui duomenu išvedimui į HTML
mygtukas1.addEventListener('click', function () {

    for (i = 0; i < figuros.length; i++) {
        console.log(figuros[i].ilgis, figuros[i].plotis, figuros[i].aukstis);
        //Sukuriu tr ir td
        var tableRow = document.createElement('tr');
        var numeris = document.createElement('td');
        var ilgisTD = document.createElement('td');
        var plotisTD = document.createElement('td');
        var aukstisTD = document.createElement('td');
        //į tbody įdedu tr ir td su duomenimis iš figuros masyvo su ojektais 
        tbody.appendChild(tableRow);
        tableRow.appendChild(numeris).innerHTML = i + 1 + ' nr.';
        tableRow.appendChild(ilgisTD).innerHTML = figuros[i].ilgis;
        tableRow.appendChild(plotisTD).innerHTML = figuros[i].plotis;
        tableRow.appendChild(aukstisTD).innerHTML = figuros[i].aukstis;
    };
})
// Turio paskaičiavimas ir max reiksmes radimas
mygtukas2.addEventListener('click', function () {
    for (i = 0; i < figuros.length; i++) {

        // Sukuriami table elementai ir atvaizduojami HTML'e tik su var numeris reiksme
        var tableRow = document.createElement('tr');
        var numeris = document.createElement('td');
        var ilgisTD = document.createElement('td');
        var plotisTD = document.createElement('td');
        var aukstisTD = document.createElement('td');
        var turisTD = document.createElement('td');
        tbody.appendChild(tableRow);
        tableRow.appendChild(numeris).innerHTML = i + 1 + ' nr.';
        tableRow.appendChild(ilgisTD);
        tableRow.appendChild(plotisTD);
        tableRow.appendChild(aukstisTD);
        // Įdedamos figurų turių reikšmės
        var turis = figuros[i].ilgis * figuros[i].plotis * figuros[i].aukstis;
        tableRow.appendChild(turisTD).innerHTML = turis;
    }
});
// Tūrio skaičiavimas, Max reiksmes radimas ir atvaizdavimas raudona spalva
mygtukas2.addEventListener('click', function(){
    var turis = figuros[0].ilgis * figuros[0].plotis * figuros[0].aukstis;
    var turis2 = figuros[1].ilgis * figuros[1].plotis * figuros[1].aukstis;
    var turis3 = figuros[2].ilgis * figuros[2].plotis * figuros[2].aukstis;
    console.log(turis, turis2, turis3);
    if (turis > turis2 > turis3) {
        console.log('didžiausias figūros turis numeriu: 1 nr.');
        document.querySelector('tr:nth-child(4)').classList.add('raudona');
    } else if (turis2 > turis3) {
        console.log('didžiausias figūros turis numeriu: 2 nr.')
        document.querySelector('tr:nth-child(5)').classList.add('raudona');
    }else{
        console.log('didžiausias figūros turis numeriu: 3 nr.')
        document.querySelector('tr:nth-child(6)').classList.add('raudona');
    }
})


