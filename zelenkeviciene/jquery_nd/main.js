$(document).ready(function () {
    
    $('#btn').on('click', function() {
        // pasiimam inputu reiksmes
        let width = $('#width').val();
        let height = $('#height').val();
        let color = $('#color').val();
        // sukuriam bloka
        let block = $('<div class=block ></div>').css({
            'background': color,
            'width': parseInt(width) * 10, 
            'height': parseInt(height) * 10
        });
        // patalpinam i konteineri
        $('#container').append(block);

        //animacija ant paspaudimo
        $('.block').on('click', function() {
            $(this).animate({
                top: '10px',
                right: '10px',
                opacity: '50%'
            }, 300, 'linear', function() {
                $(this).animate({
                    left: '10px',
                    bottom: '30px',
                    opacity: '0%'  
                }, 100);
            });
        });
    });


});

