// Pradžioje puslapio turi matytis tik mygtukas “Naujas” ir tik tuščia lentelė. Paspaudus “Naujas” turi atsirasti
// modalinis langas su trimis įvedimo laukais ir mygtukais “Saugoti” ir “Atšaukti”. Paspaudus “Saugoti” reikia
// sukurti naują įrašą ir parodyti jį lentelėje. Paspaudus “Atšaukti” - paslėpti įvedimo formą ir nieko nedaryti.
// Pridėkite mygtukus prie kiekvieno lentelės stulpelio “Taisyti” ir “Naikinti”
// Paspaudus naikinti turi atsirasti langas su klausimu “Ar tikrai norite ištrinti?” ir mygtukais “Taip” ir “Ne”.
// Paspaudus “Taip” reikia ištrinti ir perpiešti lentelę
// Paspaudus “Taisyti” - turi vėl atsirasti langas su įvedimo laukais. Paspaudus “Saugoti” pataisytos
// reikšmės turi atsirasti lentelėje
// Naudokite Bootstrap biblioteką. Atsirandantiems langams naudokite bootstrap modal componentą.
// Saugokite automobilio duomenis ne masyve, o objekte

$(document).ready(function () {
    var nr;
    var kelias;
    var laikas;
    var duom = $('#lentele tbody');

    function Auto(nr, k, l) {
        this.nr = nr;
        this.kelias = k;
        this.laikas = l;
    }

    var automobiliai = [];

    function lentele() {
        $('#lentele').css("display", "block");
        $('table tbody').empty();
        nr = $('#nr').val();
        kelias = $('#kelias').val();
        laikas = $('#laikas').val();
        // sukuriam objekta is gautu duomenu
        var auto = new Auto(nr, kelias, laikas);
        automobiliai.push(auto);
        // is masyvo statom i lentele
        for (let i = 0; i < automobiliai.length; i++) {
            var tRow = $('<tr></tr>');
            var nrTd = $('<td>' + automobiliai[i].nr + '</td>');
            var keliasTd = $('<td>' + automobiliai[i].kelias + '</td>');
            var laikasTd = $('<td>' + automobiliai[i].laikas + '</td>');
            var taisTd = $('<td><button id="taisyti" class="btn btn-primary taisyti">Taisyti</button>');
            var naikTd = $('</td><td><button id="naikinti" class="btn btn-secondary btn-danger naikinti">Naikinti</button></td>')

            tRow.append(nrTd).append(keliasTd).append(laikasTd).append(taisTd).append(naikTd);
            duom.append(tRow);
        };
        var elems = $('input');
        for (var i = 0; i < elems.length; ++i) {
            elems[i].value = '';
        }
    }

    $('#saugoti').on('click', function (e) {
        e.preventDefault();
        nr = $('#nr').val();
        kelias = $('#kelias').val();
        laikas = $('#laikas').val();
        if (!nr || !kelias || !laikas) {     //neveikia patikrinimas ifo
            alert('Užpildykite visus duomenis');
        } else {
            lentele();
        }
    });
    var rowIndex;
    var trinama;
    $(duom).on("click", '.naikinti', function () {
        trinama = $(this).parent().parent();
        rowIndex = trinama.index();
        $('#exampleModal1').modal('show');


    });
    $('#exampleModal1').on('click', '#taip', function () {
        automobiliai.splice(rowIndex, 1);
        trinama.remove();
        console.log('istrinta');
        console.log(automobiliai);
    });
    //    Kaip isnaikinti objekta is masyvo?
    //    kaip susieti trinama eilute su objektu masyve?
});