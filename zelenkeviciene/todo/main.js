var add = document.getElementById('add');
var input = document.getElementById('userinput');
var ul = document.querySelector('ul');


//ziurim ar ivede
function inputLength() {
    return input.value.length;
}
//add mygtukas
function createListElement() {
    if (inputLength() > 0 ) {
        let li = document.createElement('li');
        li.appendChild(document.createTextNode(input.value));
        ul.appendChild(li);
        input.value = "";
        li.addEventListener('click', function(){
            li.classList.toggle('done');
        });
    
        //dar mygtuka istrynimui sukuriam
        let btn = document.createElement('button');
        btn.classList.add('remove');
        btn.appendChild(document.createTextNode("remove"));
        li.appendChild(btn);
        btn.addEventListener('click', function(){
           btn.parentElement.remove();
        });

    } else {
        alert ('Use your keyboard to enter');
    }
}

add.addEventListener('click', function(){
    createListElement();
})

//klaviaturos enter
function addListAfterKeypress(event) {
    if (inputLength() > 0 && event.key === 'Enter') {
        createListElement();
    } 
}

input.addEventListener('keypress', addListAfterKeypress);