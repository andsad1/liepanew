// Pražioje sukurti 2 input fieldus(automobilio pavadinimas ir greitis), įvedus informaciją ir paspaudus pridėti - sudėti duomenis į lentelę arba paprastą div'ą(kiekvieną kart paspaudus prisideda vis nauji duomenys, seni neišsitrina).
// Žemiau sukurti input fieldą laikas, ir paspaudus mygtuką lenktyniauti, palyginti automobilių nuvažiuotą atstumą lentelėje).
// Automobilių sukūrimui panaudoti funkciją - konstruktorių.
// ** sudedant duomenis į lentelę, paryškinti didžiausią atstumą nuvažiavusį automobilį.

// Nuotraukuoje pavizdys kaip galėtų atrodyti, kad lengviau įsivaizduoti.
function Auto(p, g) {
    this.duomPav = p;
    this.duomGrei = g;
}
var automobiliai = [];
// automobiliai[0].Auto.prototype.atstumas = atstumas;
// Psiemam visą formą
var visaForma = document.getElementById('duomenys');
var autoInfo = document.getElementById('auto-info');
var lenktyniauti = document.getElementById('lenktyniauti');


visaForma.addEventListener('submit', function(e) {
    e.preventDefault();

    // Pasiemam duomenis is inputu
    var duomPav = document.getElementById('pavadinimas').value;
    var duomGrei = document.getElementById('greitis').value;
    // Sukuriam automobilio objekta, pagal Auto konstruktoriu
    var auto = new Auto(duomPav, duomGrei);


    // sudedam automobilius i masyva

    automobiliai.push(auto);
    // sukuriam pavadinimo div'a
    var pavadinimasDiv = document.createElement('div');

    // idedam teksta i pavadinimo div'a
    pavadinimasDiv.innerHTML = duomPav;
    // sukuriam greicio div'a
    var greitisDiv = document.createElement('div');

    // idedam teksta i greicio div'a
    greitisDiv.innerHTML = duomGrei;
    // susikuriam auto row div'a
    var autoRow = document.createElement('div');
    // uzdedam klase auto row div'ui, kad galetume ji pastiliuot
    autoRow.classList.add('auto-row');
    // sudedam pavadinima ir greiti i auto row diva
    autoRow.appendChild(pavadinimasDiv);
    autoRow.appendChild(greitisDiv);
    // sudedam pilna auto row div'a i autoinfo div'a
    autoInfo.appendChild(autoRow);
    document.getElementById('pavadinimas').value = '';
    document.getElementById('greitis').value = '';
    console.log(duomGrei);



});

var rezultatai = document.getElementById('rezultatai');
lenktyniauti.addEventListener('submit', function(e) {
    e.preventDefault();
    // Pasiemam ivesta laika
    var laikas = document.getElementById('laikas').value;
    var atstumai = [];
    // Suskaiciuojam atstumus
    for (let i = 0; i < automobiliai.length; i++) {
        var atstumas = parseInt(laikas) * parseInt(automobiliai[i].duomGrei);
        atstumai.push(atstumas);
        console.log(atstumas);
        console.log(atstumai);
    }

    // Randam didziausia reiksme
    var max = Math.max(...atstumai);
    for (let j = 0; j < atstumai.length; j++) {
        var pavadinimasDiv = document.createElement('div');
        pavadinimasDiv.innerHTML = automobiliai[j].duomPav;
        var atstumDiv = document.createElement('div');
        atstumDiv.innerHTML = atstumai[j];
        var autoRow = document.createElement('div');
        // Patikrinam kurie atstumai sutampta su didziausia reiksme
        if (atstumai[j] == max) {
            // Sita klase kuriam del stiliavimo
            autoRow.classList.add('greiciausias');
        }
        autoRow.classList.add('auto-row');
        // sudedam pavadinima ir greiti i auto row diva
        autoRow.appendChild(pavadinimasDiv);
        autoRow.appendChild(atstumDiv);
        rezultatai.appendChild(autoRow);
    }

    // Pasiemam ivesta laika

    // Suskaiciuojam atstumus

    // Randam didziausia reiksme
    // var max = Math.max(...atstumai);
    // for (let j = 0; j < atstumai.length; j++) {
    //     var pavDiv = document.createElement('div');
    //     pavDiv.innerHTML = automobiliai[j].pavadinimas;
    //     var atstumDiv = document.createElement('div');
    //     atstumDiv.innerHTML = atstumai[j];
    //     var autoRow = document.createElement('div');
    //     // Patikrinam kurie atstumai sutampta su didziausia reiksme
    //     if (atstumai[j] == max) {
    //         // Sita klase kuriam del stiliavimo
    //         autoRow.classList.add('greiciausias');
    //     }
    //     autoRow.classList.add('auto-row');
    //     // sudedam pavadinima ir greiti i auto row diva
    //     autoRow.appendChild(pavDiv);
    //     autoRow.appendChild(atstumDiv);
    //     rezultatai.appendChild(autoRow);
    // }





    // for (let i = 0; i < automobiliai.length; i++) {
    //     var atstumas = parseInt(laikas) * parseInt(automobiliai[i].greitis);
    //     atstumai.push(atstumas);
    // }



    // var max = Math.max(...atstumai);
    // for (let j = 0; j < atstumai.length; j++) {
    //     var pavDiv = document.createElement('div');
    //     pavDiv.innerHTML = automobiliai[j].pavadinimas;
    //     var atstumDiv = document.createElement('div');
    //     atstumDiv.innerHTML = atstumai[j];
    //     var autoRow = document.createElement('div');
    //     // Patikrinam kurie atstumai sutampta su didziausia reiksme
    //     if (atstumai[j] == max) {
    //         // Sita klase kuriam del stiliavimo
    //         autoRow.classList.add('greiciausias');
    //     }
    //     autoRow.classList.add('auto-row');
    //     // sudedam pavadinima ir greiti i auto row diva
    //     autoRow.appendChild(pavDiv);
    //     autoRow.appendChild(atstumDiv);
    //     rezultatai.appendChild(autoRow);
    // }

});