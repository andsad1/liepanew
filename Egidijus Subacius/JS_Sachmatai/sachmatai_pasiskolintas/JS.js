// JS failas yra pasiskolintas is geriausio draugo google :)
// Uzduoti darau dviem budais : 1) google visagalis
// 2) bandau pats. (bandau pats esu pasidares panasiai tiek kiek dest pvz, tik visa lenta ir viskas gyva ( t.y veikia vaiksciojimas,
// ir kirtimai, tik niekaip neikertu su tiksliom ejimo pozicijom, tai jeigu kas pasidares ir nori pasidalinti mintis.
// parasykit :) 2 versija ikelsiu pirmadieni ryte :) gal dar ka isgimdysiu :)  ))

var main = {
    variables: {
        turn: "w",
        selectedpiece: "",
        highlighted: [],
        pieces: {
            w_king: { position: "5_1", img: "&#9812;", captured: !1, moved: !1, type: "w_king" },
            w_queen: { position: "4_1", img: "&#9813;", captured: !1, moved: !1, type: "w_queen" },
            w_bishop1: { position: "3_1", img: "&#9815;", captured: !1, moved: !1, type: "w_bishop" },
            w_bishop2: { position: "6_1", img: "&#9815;", captured: !1, moved: !1, type: "w_bishop" },
            w_knight1: { position: "2_1", img: "&#9816;", captured: !1, moved: !1, type: "w_knight" },
            w_knight2: {
                position: "7_1",
                img: "&#9816;",
                captured: !1,
                moved: !1,
                type: "w_knight"
            },
            w_rook1: { position: "1_1", img: "&#9814;", captured: !1, moved: !1, type: "w_rook" },
            w_rook2: { position: "8_1", img: "&#9814;", captured: !1, moved: !1, type: "w_rook" },
            w_pawn1: { position: "1_2", img: "&#9817;", captured: !1, type: "w_pawn", moved: !1 },
            w_pawn2: { position: "2_2", img: "&#9817;", captured: !1, type: "w_pawn", moved: !1 },
            w_pawn3: { position: "3_2", img: "&#9817;", captured: !1, type: "w_pawn", moved: !1 },
            w_pawn4: { position: "4_2", img: "&#9817;", captured: !1, type: "w_pawn", moved: !1 },
            w_pawn5: {
                position: "5_2",
                img: "&#9817;",
                captured: !1,
                type: "w_pawn",
                moved: !1
            },
            w_pawn6: { position: "6_2", img: "&#9817;", captured: !1, type: "w_pawn", moved: !1 },
            w_pawn7: { position: "7_2", img: "&#9817;", captured: !1, type: "w_pawn", moved: !1 },
            w_pawn8: { position: "8_2", img: "&#9817;", captured: !1, type: "w_pawn", moved: !1 },
            b_king: { position: "5_8", img: "&#9818;", captured: !1, moved: !1, type: "b_king" },
            b_queen: { position: "4_8", img: "&#9819;", captured: !1, moved: !1, type: "b_queen" },
            b_bishop1: { position: "3_8", img: "&#9821;", captured: !1, moved: !1, type: "b_bishop" },
            b_bishop2: {
                position: "6_8",
                img: "&#9821;",
                captured: !1,
                moved: !1,
                type: "b_bishop"
            },
            b_knight1: { position: "2_8", img: "&#9822;", captured: !1, moved: !1, type: "b_knight" },
            b_knight2: { position: "7_8", img: "&#9822;", captured: !1, moved: !1, type: "b_knight" },
            b_rook1: { position: "1_8", img: "&#9820;", captured: !1, moved: !1, type: "b_rook" },
            b_rook2: { position: "8_8", img: "&#9820;", captured: !1, moved: !1, type: "b_rook" },
            b_pawn1: { position: "1_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 },
            b_pawn2: { position: "2_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 },
            b_pawn3: { position: "3_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 },
            b_pawn4: { position: "4_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 },
            b_pawn5: { position: "5_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 },
            b_pawn6: { position: "6_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 },
            b_pawn7: { position: "7_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 },
            b_pawn8: { position: "8_7", img: "&#9823;", captured: !1, type: "b_pawn", moved: !1 }
        }
    },
    methods: {
        gamesetup: function() {
            $(".gamecell").attr("chess",
                "null");
            for (var d in main.variables.pieces) $("#" + main.variables.pieces[d].position).html(main.variables.pieces[d].img), $("#" + main.variables.pieces[d].position).attr("chess", d)
        },
        moveoptions: function(d) {
            var b = { x: "", y: "" };
            b.x = main.variables.pieces[d].position.split("_")[0];
            b.y = main.variables.pieces[d].position.split("_")[1];
            var a = [];
            a = [];
            var c = main.variables.pieces[d].position;
            0 != main.variables.highlighted.length && main.methods.togglehighlight(main.variables.highlighted);
            switch (main.variables.pieces[d].type) {
                case "w_king":
                    a =
                        "null" == $("#6_1").attr("chess") && "null" == $("#7_1").attr("chess") && 0 == main.variables.pieces.w_king.moved && 0 == main.variables.pieces.w_rook2.moved ? [{ x: 1, y: 1 }, { x: 1, y: 0 }, { x: 1, y: -1 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 0 }, { x: -1, y: 1 }, { x: 0, y: 1 }, { x: 2, y: 0 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) }) : [{ x: 1, y: 1 }, { x: 1, y: 0 }, { x: 1, y: -1 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 0 }, { x: -1, y: 1 }, { x: 0, y: 1 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) });
                    a = main.methods.options(c, a, main.variables.pieces[d].type).slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "b_king":
                    a = "null" == $("#6_8").attr("chess") && "null" == $("#7_8").attr("chess") && 0 == main.variables.pieces.b_king.moved && 0 == main.variables.pieces.b_rook2.moved ? [{ x: 1, y: 1 }, { x: 1, y: 0 }, { x: 1, y: -1 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 0 }, { x: -1, y: 1 }, { x: 0, y: 1 }, { x: 2, y: 0 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) }) : [{ x: 1, y: 1 }, {
                        x: 1,
                        y: 0
                    }, { x: 1, y: -1 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 0 }, { x: -1, y: 1 }, { x: 0, y: 1 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) });
                    a = main.methods.options(c, a, main.variables.pieces[d].type).slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "w_queen":
                    d = main.methods.w_options(b, [{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }, { x: 4, y: 4 }, { x: 5, y: 5 }, { x: 6, y: 6 }, { x: 7, y: 7 }]);
                    a = main.methods.w_options(b, [{ x: 1, y: -1 }, { x: 2, y: -2 }, { x: 3, y: -3 }, { x: 4, y: -4 }, { x: 5, y: -5 }, {
                        x: 6,
                        y: -6
                    }, { x: 7, y: -7 }]);
                    c = main.methods.w_options(b, [{ x: -1, y: 1 }, { x: -2, y: 2 }, { x: -3, y: 3 }, { x: -4, y: 4 }, { x: -5, y: 5 }, { x: -6, y: 6 }, { x: -7, y: 7 }]);
                    var e = main.methods.w_options(b, [{ x: -1, y: -1 }, { x: -2, y: -2 }, { x: -3, y: -3 }, { x: -4, y: -4 }, { x: -5, y: -5 }, { x: -6, y: -6 }, { x: -7, y: -7 }]);
                    var g = main.methods.w_options(b, [{ x: 1, y: 0 }, { x: 2, y: 0 }, { x: 3, y: 0 }, { x: 4, y: 0 }, { x: 5, y: 0 }, { x: 6, y: 0 }, { x: 7, y: 0 }]);
                    var h = main.methods.w_options(b, [{ x: 0, y: 1 }, { x: 0, y: 2 }, { x: 0, y: 3 }, { x: 0, y: 4 }, { x: 0, y: 5 }, { x: 0, y: 6 }, { x: 0, y: 7 }]);
                    var k = main.methods.w_options(b, [{ x: -1, y: 0 }, {
                        x: -2,
                        y: 0
                    }, { x: -3, y: 0 }, { x: -4, y: 0 }, { x: -5, y: 0 }, { x: -6, y: 0 }, { x: -7, y: 0 }]);
                    var l = main.methods.w_options(b, [{ x: 0, y: -1 }, { x: 0, y: -2 }, { x: 0, y: -3 }, { x: 0, y: -4 }, { x: 0, y: -5 }, { x: 0, y: -6 }, { x: 0, y: -7 }]);
                    a = d.concat(a).concat(c).concat(e).concat(g).concat(h).concat(k).concat(l);
                    a = a.slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "b_queen":
                    d = main.methods.b_options(b, [{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }, { x: 4, y: 4 }, { x: 5, y: 5 }, { x: 6, y: 6 }, { x: 7, y: 7 }]);
                    a = main.methods.b_options(b, [{ x: 1, y: -1 }, { x: 2, y: -2 },
                        { x: 3, y: -3 }, { x: 4, y: -4 }, { x: 5, y: -5 }, { x: 6, y: -6 }, { x: 7, y: -7 }
                    ]);
                    c = main.methods.b_options(b, [{ x: -1, y: 1 }, { x: -2, y: 2 }, { x: -3, y: 3 }, { x: -4, y: 4 }, { x: -5, y: 5 }, { x: -6, y: 6 }, { x: -7, y: 7 }]);
                    e = main.methods.b_options(b, [{ x: -1, y: -1 }, { x: -2, y: -2 }, { x: -3, y: -3 }, { x: -4, y: -4 }, { x: -5, y: -5 }, { x: -6, y: -6 }, { x: -7, y: -7 }]);
                    g = main.methods.b_options(b, [{ x: 1, y: 0 }, { x: 2, y: 0 }, { x: 3, y: 0 }, { x: 4, y: 0 }, { x: 5, y: 0 }, { x: 6, y: 0 }, { x: 7, y: 0 }]);
                    h = main.methods.b_options(b, [{ x: 0, y: 1 }, { x: 0, y: 2 }, { x: 0, y: 3 }, { x: 0, y: 4 }, { x: 0, y: 5 }, { x: 0, y: 6 }, { x: 0, y: 7 }]);
                    k = main.methods.b_options(b, [{ x: -1, y: 0 }, { x: -2, y: 0 }, { x: -3, y: 0 }, { x: -4, y: 0 }, { x: -5, y: 0 }, { x: -6, y: 0 }, { x: -7, y: 0 }]);
                    l = main.methods.b_options(b, [{ x: 0, y: -1 }, { x: 0, y: -2 }, { x: 0, y: -3 }, { x: 0, y: -4 }, { x: 0, y: -5 }, { x: 0, y: -6 }, { x: 0, y: -7 }]);
                    a = d.concat(a).concat(c).concat(e).concat(g).concat(h).concat(k).concat(l);
                    a = a.slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "w_bishop":
                    d = main.methods.w_options(b, [{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }, { x: 4, y: 4 }, { x: 5, y: 5 }, { x: 6, y: 6 }, { x: 7, y: 7 }]);
                    a = main.methods.w_options(b, [{ x: 1, y: -1 },
                        { x: 2, y: -2 }, { x: 3, y: -3 }, { x: 4, y: -4 }, { x: 5, y: -5 }, { x: 6, y: -6 }, { x: 7, y: -7 }
                    ]);
                    c = main.methods.w_options(b, [{ x: -1, y: 1 }, { x: -2, y: 2 }, { x: -3, y: 3 }, { x: -4, y: 4 }, { x: -5, y: 5 }, { x: -6, y: 6 }, { x: -7, y: 7 }]);
                    e = main.methods.w_options(b, [{ x: -1, y: -1 }, { x: -2, y: -2 }, { x: -3, y: -3 }, { x: -4, y: -4 }, { x: -5, y: -5 }, { x: -6, y: -6 }, { x: -7, y: -7 }]);
                    a = d.concat(a).concat(c).concat(e);
                    a = a.slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "b_bishop":
                    d = main.methods.b_options(b, [{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }, { x: 4, y: 4 }, {
                        x: 5,
                        y: 5
                    }, { x: 6, y: 6 }, { x: 7, y: 7 }]);
                    a = main.methods.b_options(b, [{ x: 1, y: -1 }, { x: 2, y: -2 }, { x: 3, y: -3 }, { x: 4, y: -4 }, { x: 5, y: -5 }, { x: 6, y: -6 }, { x: 7, y: -7 }]);
                    c = main.methods.b_options(b, [{ x: -1, y: 1 }, { x: -2, y: 2 }, { x: -3, y: 3 }, { x: -4, y: 4 }, { x: -5, y: 5 }, { x: -6, y: 6 }, { x: -7, y: 7 }]);
                    e = main.methods.b_options(b, [{ x: -1, y: -1 }, { x: -2, y: -2 }, { x: -3, y: -3 }, { x: -4, y: -4 }, { x: -5, y: -5 }, { x: -6, y: -6 }, { x: -7, y: -7 }]);
                    a = d.concat(a).concat(c).concat(e);
                    a = a.slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "w_knight":
                    a = [{
                        x: -1,
                        y: 2
                    }, { x: 1, y: 2 }, { x: 1, y: -2 }, { x: -1, y: -2 }, { x: 2, y: 1 }, { x: 2, y: -1 }, { x: -2, y: -1 }, { x: -2, y: 1 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) });
                    a = main.methods.options(c, a, main.variables.pieces[d].type).slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "b_knight":
                    a = [{ x: -1, y: 2 }, { x: 1, y: 2 }, { x: 1, y: -2 }, { x: -1, y: -2 }, { x: 2, y: 1 }, { x: 2, y: -1 }, { x: -2, y: -1 }, { x: -2, y: 1 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) });
                    a = main.methods.options(c, a, main.variables.pieces[d].type).slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "w_rook":
                    d = main.methods.w_options(b, [{ x: 1, y: 0 }, { x: 2, y: 0 }, { x: 3, y: 0 }, { x: 4, y: 0 }, { x: 5, y: 0 }, { x: 6, y: 0 }, { x: 7, y: 0 }]);
                    a = main.methods.w_options(b, [{ x: 0, y: 1 }, { x: 0, y: 2 }, { x: 0, y: 3 }, { x: 0, y: 4 }, { x: 0, y: 5 }, { x: 0, y: 6 }, { x: 0, y: 7 }]);
                    c = main.methods.w_options(b, [{ x: -1, y: 0 }, { x: -2, y: 0 }, { x: -3, y: 0 }, { x: -4, y: 0 }, { x: -5, y: 0 }, { x: -6, y: 0 }, { x: -7, y: 0 }]);
                    e = main.methods.w_options(b, [{ x: 0, y: -1 },
                        { x: 0, y: -2 }, { x: 0, y: -3 }, { x: 0, y: -4 }, { x: 0, y: -5 }, { x: 0, y: -6 }, { x: 0, y: -7 }
                    ]);
                    a = d.concat(a).concat(c).concat(e);
                    a = a.slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "b_rook":
                    d = main.methods.b_options(b, [{ x: 1, y: 0 }, { x: 2, y: 0 }, { x: 3, y: 0 }, { x: 4, y: 0 }, { x: 5, y: 0 }, { x: 6, y: 0 }, { x: 7, y: 0 }]);
                    a = main.methods.b_options(b, [{ x: 0, y: 1 }, { x: 0, y: 2 }, { x: 0, y: 3 }, { x: 0, y: 4 }, { x: 0, y: 5 }, { x: 0, y: 6 }, { x: 0, y: 7 }]);
                    c = main.methods.b_options(b, [{ x: -1, y: 0 }, { x: -2, y: 0 }, { x: -3, y: 0 }, { x: -4, y: 0 }, { x: -5, y: 0 }, { x: -6, y: 0 }, {
                        x: -7,
                        y: 0
                    }]);
                    e = main.methods.b_options(b, [{ x: 0, y: -1 }, { x: 0, y: -2 }, { x: 0, y: -3 }, { x: 0, y: -4 }, { x: 0, y: -5 }, { x: 0, y: -6 }, { x: 0, y: -7 }]);
                    a = d.concat(a).concat(c).concat(e);
                    a = a.slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "w_pawn":
                    0 == main.variables.pieces[d].moved ? a = [{ x: 0, y: 1 }, { x: 0, y: 2 }, { x: 1, y: 1 }, { x: -1, y: 1 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) }) : 1 == main.variables.pieces[d].moved && (a = [{ x: 0, y: 1 }, { x: 1, y: 1 }, { x: -1, y: 1 }].map(function(f) {
                        return parseInt(b.x) +
                            parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y))
                    }));
                    a = main.methods.options(c, a, main.variables.pieces[d].type).slice(0);
                    main.variables.highlighted = a.slice(0);
                    main.methods.togglehighlight(a);
                    break;
                case "b_pawn":
                    0 == main.variables.pieces[d].moved ? a = [{ x: 0, y: -1 }, { x: 0, y: -2 }, { x: 1, y: -1 }, { x: -1, y: -1 }].map(function(f) { return parseInt(b.x) + parseInt(f.x) + "_" + (parseInt(b.y) + parseInt(f.y)) }) : 1 == main.variables.pieces[d].moved && (a = [{ x: 0, y: -1 }, { x: 1, y: -1 }, { x: -1, y: -1 }].map(function(f) {
                        return parseInt(b.x) + parseInt(f.x) +
                            "_" + (parseInt(b.y) + parseInt(f.y))
                    })), a = main.methods.options(c, a, main.variables.pieces[d].type).slice(0), main.variables.highlighted = a.slice(0), main.methods.togglehighlight(a)
            }
        },
        options: function(d, b, a) {
            b = b.filter(function(c) { var e = parseInt(c.split("_")[0]); var g = parseInt(c.split("_")[1]); if (!(1 > e || 8 < e || 1 > g || 8 < g)) return c });
            switch (a) {
                case "w_king":
                    b = b.filter(function(c) { return "null" == $("#" + c).attr("chess") || "b" == $("#" + c).attr("chess").slice(0, 1) });
                    break;
                case "b_king":
                    b = b.filter(function(c) {
                        return "null" ==
                            $("#" + c).attr("chess") || "w" == $("#" + c).attr("chess").slice(0, 1)
                    });
                    break;
                case "w_knight":
                    b = b.filter(function(c) { return "null" == $("#" + c).attr("chess") || "b" == $("#" + c).attr("chess").slice(0, 1) });
                    break;
                case "b_knight":
                    b = b.filter(function(c) { return "null" == $("#" + c).attr("chess") || "w" == $("#" + c).attr("chess").slice(0, 1) });
                    break;
                case "w_pawn":
                    b = b.filter(function(c) {
                        var e = c.split("_");
                        var g = d.split("_")[0];
                        var h = d.split("_")[1];
                        if (e[0] < g || e[0] > g) return "null" != $("#" + c).attr("chess") && "b" == $("#" + c).attr("chess").slice(0,
                            1);
                        if (e[1] != parseInt(h) + 2 || "null" == $("#" + g + "_" + (parseInt(h) + 1)).attr("chess")) return "null" == $("#" + c).attr("chess")
                    });
                    break;
                case "b_pawn":
                    b = b.filter(function(c) { var e = c.split("_"); var g = d.split("_")[0]; var h = d.split("_")[1]; if (e[0] < g || e[0] > g) return "null" != $("#" + c).attr("chess") && "w" == $("#" + c).attr("chess").slice(0, 1); if (e[1] != parseInt(h) - 2 || "null" == $("#" + g + "_" + (parseInt(h) - 1)).attr("chess")) return "null" == $("#" + c).attr("chess") })
            }
            return b
        },
        w_options: function(d, b) {
            var a = !1;
            return b = b.map(function(c) {
                return parseInt(d.x) +
                    parseInt(c.x) + "_" + (parseInt(d.y) + parseInt(c.y))
            }).filter(function(c) { var e = parseInt(c.split("_")[0]); var g = parseInt(c.split("_")[1]); if (!(1 > e || 8 < e || 1 > g || 8 < g)) return c }).filter(function(c) { if (0 == a) { if ("null" == $("#" + c).attr("chess")) return console.log(c), c; if ("b" == $("#" + c).attr("chess").slice(0, 1)) return a = !0, console.log(c), c; "w" == $("#" + c).attr("chess").slice(0, 1) && (console.log(c + "-3"), a = !0) } })
        },
        b_options: function(d, b) {
            var a = !1;
            return b = b.map(function(c) {
                return parseInt(d.x) + parseInt(c.x) + "_" + (parseInt(d.y) +
                    parseInt(c.y))
            }).filter(function(c) { var e = parseInt(c.split("_")[0]); var g = parseInt(c.split("_")[1]); if (!(1 > e || 8 < e || 1 > g || 8 < g)) return c }).filter(function(c) { if (0 == a) { if ("null" == $("#" + c).attr("chess")) return c; if ("w" == $("#" + c).attr("chess").slice(0, 1)) return a = !0, c; "b" == $("#" + c).attr("chess").slice(0, 1) && (a = !0) } })
        },
        capture: function(d) {
            var b = $("#" + main.variables.selectedpiece).attr("chess"),
                a = main.variables.selectedpiece;
            $("#" + d.id).html(main.variables.pieces[b].img);
            $("#" + d.id).attr("chess", b);
            $("#" +
                a).html("");
            $("#" + a).attr("chess", "null");
            main.variables.pieces[b].position = d.id;
            main.variables.pieces[b].moved = !0;
            main.variables.pieces[d.name].captured = !0
        },
        move: function(d) {
            var b = $("#" + main.variables.selectedpiece).attr("chess");
            $("#" + d.id).html(main.variables.pieces[b].img);
            $("#" + d.id).attr("chess", b);
            $("#" + main.variables.selectedpiece).html("");
            $("#" + main.variables.selectedpiece).attr("chess", "null");
            main.variables.pieces[b].position = d.id;
            main.variables.pieces[b].moved = !0
        },
        endturn: function() {
            "w" ==
            main.variables.turn ? (main.variables.turn = "b", main.methods.togglehighlight(main.variables.highlighted), main.variables.highlighted.length = 0, main.variables.selectedpiece = "", $("#turn").html("It's Blacks Turn")) : (main.variables.turn = "b", main.variables.turn = "w", main.methods.togglehighlight(main.variables.highlighted), main.variables.highlighted.length = 0, main.variables.selectedpiece = "", $("#turn").html("It's Whites Turn"));
            $("#turn").addClass("turnhighlight");
            window.setTimeout(function() { $("#turn").removeClass("turnhighlight") },
                1500)
        },
        togglehighlight: function(d) { d.forEach(function(b, a, c) { $("#" + b).toggleClass("green shake-little neongreen_txt") }) }
    }
};
$(document).ready(function() {
    main.methods.gamesetup();
    $(".gamecell").click(function(d) {
        var b = main.variables.selectedpiece;
        var a = "" == main.variables.selectedpiece ? $("#" + d.target.id).attr("chess") : $("#" + main.variables.selectedpiece).attr("chess");
        var c = { name: $(this).attr("chess"), id: d.target.id };
        if ("" == main.variables.selectedpiece && c.name.slice(0, 1) == main.variables.turn) main.variables.selectedpiece = d.target.id, main.methods.moveoptions($(this).attr("chess"));
        else if ("" != main.variables.selectedpiece &&
            "null" == c.name) {
            if ("w_king" == a || "b_king" == a) {
                d = a = "b_king";
                a = 0 == main.variables.pieces[a].moved;
                b = 0 == main.variables.pieces.b_rook2.moved;
                var e = 0 == main.variables.pieces.w_rook2.moved,
                    g = "7_8" == c.id,
                    h = "7_1" == c.id;
                a && e && h ? (main.variables.pieces.w_king.position = "7_1", main.variables.pieces.w_king.moved = !0, $("#5_1").html(""), $("#5_1").attr("chess", "null"), $("#7_1").html(main.variables.pieces.w_king.img), $("#7_1").attr("chess", "w_king"), main.variables.pieces.w_rook2.position = "6_1", main.variables.pieces.w_rook2.moved = !0, $("#8_1").html(""), $("#8_1").attr("chess", "null"), $("#6_1").html(main.variables.pieces.w_rook2.img), $("#6_1").attr("chess", "w_rook2")) : d && a && b && g ? (main.variables.pieces.b_king.position = "7_8", main.variables.pieces.b_king.moved = !0, $("#5_8").html(""), $("#5_8").attr("chess", "null"), $("#7_8").html(main.variables.pieces.b_king.img), $("#7_8").attr("chess", "b_king"), main.variables.pieces.b_rook2.position = "6_8", main.variables.pieces.b_rook2.moved = !0, $("#8_8").html(""), $("#8_8").attr("chess", "null"), $("#6_8").html(main.variables.pieces.b_rook2.img),
                    $("#6_8").attr("chess", "b_rook2")) : main.methods.move(c)
            } else main.methods.move(c);
            main.methods.endturn()
        } else "" != main.variables.selectedpiece && "null" != c.name && c.id != b && a.slice(0, 1) != c.name.slice(0, 1) ? b != c.id && -1 != main.variables.highlighted.indexOf(c.id) && (main.methods.capture(c), main.methods.endturn()) : "" != main.variables.selectedpiece && "null" != c.name && c.id != b && a.slice(0, 1) == c.name.slice(0, 1) && (main.methods.togglehighlight(main.variables.highlighted), main.variables.highlighted.length = 0, main.variables.selectedpiece =
            c.id, main.methods.moveoptions(c.name))
    });
    $("body").contextmenu(function(d) { d.preventDefault() })
});