const draggables = document.querySelectorAll('.piece')
const containers = document.querySelectorAll('.cell_1', '.cell_2')

draggables.forEach(piece => {
    piece.addEventListener('dragstart', () => {
        piece.classList.add('dragging')  
})

piece.addEventListener('dragend', () =>{
    piece.classList.add('dragging')
})

containers.forEach(cell_1 => {
    cell_1.addEventListener('dragover', e =>{
        e.preventDefault()
        const piece = document.querySelector('.dragging')
        containers.appendChild(piece)
    })
})   

containers.forEach(cell_2 => {
    cell_1.addEventListener('dragover', e =>{
        e.preventDefault()
        const piece = document.querySelector('.dragging')
        containers.appendChild(piece)
    })
})  
})