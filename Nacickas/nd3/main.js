var id, parentId;

            function drag(e) {
                // console.log(e);
                id = e.target.id; // atsiminti elemento id
                // console.log(id);
                // console.log(e.target);
                parentId = e.target.parentElement.id; //atsimenam tevinio elemento(langelio) id
                // console.log(parentId);
                // console.log(parentId);
                // e.dataTransfer.setData("text", id);
                // console.log('dragstart');
            }
            function allowDrop(e) {
                // console.log(e.target.id);
                // jei tas pats arba jo tevas arba a2 tai neleidžiame "numesti"
                if (e.target.id == id || e.target.id == parentId) return;
                // console.log('dragover');
                e.preventDefault();     // reikia kviesti šitą funkciją kad leisti "numesti" elementą
            }   
            function drop(e) {
                e.preventDefault();     // uždraudžiam standartinį naršyklės veikimą su elementu - kaip kad mygtuko paspaudimas ir pan.
                // if (id != e.target.id) {
                //     console.log(e.target.id);
                //     // document.getElementById(e.target.id).parentElement.removeChild(e.target.id);
                //     var parent = document.getElementById(e.target.parentElement.id);
                //     console.log(parent.childNodes);
                //     parent.removeChild(parent.childNodes[0]);
                // }
                var element = document.getElementById("gamefigure");  // randame elementą
                // console.log(element);
                e.target.appendChild(element);

                // sios dvi eilutes darom visiskai ta pati, tik skiriasi parasymas
                // tbody.appendChild('<tr></tr>');
                // tbody.innerHTML = '<tr></tr>';
                

                var steps = document.getElementById('steps');
                // funkcija sukurti naujam html elementui
                var step = document.createElement("div");
                // console.log(step);
                step.innerHTML = parentId + '-' + e.target.id;
                steps.appendChild(step);
                // console.log('ondrop');
            }